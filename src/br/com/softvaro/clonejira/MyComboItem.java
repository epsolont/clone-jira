package br.com.softvaro.clonejira;
/**
 * @author colaborador
 *
 */
public class MyComboItem implements Comparable<MyComboItem>{
	private String key;
	private String value;
	/**
	 * @param key
	 * @param value
	 */
	public MyComboItem(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.key;
	}
	
    @Override
    public boolean equals(Object obj) {
    	if (obj == null || this.key == null) {
    		return false;
    	}
    	if (!(obj instanceof MyComboItem)) {
    		return false;
    	}
    	return this.key.equals(((MyComboItem)obj).getKey());
    }
	/**
	 * @param o
	 * @return
	 */
	public int compareTo(MyComboItem o) {
		return this.key.compareTo(o.getKey());
	}
	
}
	