package br.com.softvaro.clonejira;

/**
 * Class to represent data from a Jira issue
 * 
 * @author colaborador
 *
 */
public class FieldsHdi extends Fields {

    /*
     * {\"fields\": {\"project\":{ \"key\": \"TIS\"}, \"summary\": \"SISCAS-12377 Teste usando a API REST do Jira\",
     * \"description\": \"Criando uma tarefa usando a API REST do Jira\", \"status\": {\"id\": \"10100\"}, \"issuetype\": {\"name\": \"Task\"},
     * \"customfield_10006\": \"TIS-1\" } }
     */

    // link para jira externo
    private String customfield_10300;

    public FieldsHdi() {
	super();
	Assignee assignee = new Assignee("projetos.softvaro");
	this.setAssignee(assignee);
    }

    @Override
    public void setLinkJiraExterno(String linkJiraExterno) {
	this.customfield_10300 = linkJiraExterno;
    }

    /**
     * @return the customfield_10300
     */
    public String getCustomfield_10300() {
	return customfield_10300;
    }

    /**
     * @param customfield_10300
     *            the customfield_10300 to set
     */
    public void setCustomfield_10300(String customfield_10300) {
	this.customfield_10300 = customfield_10300;
    }

    @Override
    public void setEpicName(String epicName) {
	// does nothing epic name is supposed to be used only by epic links
    }
}
