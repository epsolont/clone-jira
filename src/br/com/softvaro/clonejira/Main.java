package br.com.softvaro.clonejira;

import java.awt.EventQueue;
import java.util.Properties;

import br.com.softvaro.clonejira.util.CypherUtil;
import br.com.softvaro.clonejira.util.PropertyUtil;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Properties p = PropertyUtil.getProperties();
					CypherUtil c = new CypherUtil(p);
					p = c.getProperties();
					MainUI window = new MainUI(c);					
					window.show(p);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
