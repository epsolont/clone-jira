package br.com.softvaro.clonejira.util;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

/**
 * @author colaborador
 *
 */
public class CypherUtil {
	private java.util.Properties p;
    private static final String ALGORITHM = "AES";
    private static Key key = null;
    private static Cipher cipher = null;
    
	/**
	 * @param p Properties
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 */
    public CypherUtil(Properties p) throws NoSuchAlgorithmException, NoSuchPaddingException {
    	super();
    	initialize(p);
    }
    
    /**
	 * @return the key
	 */
	public static Key getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public static void setKey(Key key) {
		CypherUtil.key = key;
	}

	/**
	 * @return the p
	 */
	public java.util.Properties getProperties() {
		return p;
	}

	/**
	 * @param p the p to set
	 */
	public void setProperties(java.util.Properties p) {
		this.p = p;
	}

	/**
     * @param p
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public void initialize(Properties p) throws NoSuchAlgorithmException, NoSuchPaddingException {
    	this.p = p;
    	cipher = Cipher.getInstance(ALGORITHM);
    	String base64Key = p.getProperty("key");
		if (StringUtils.isNotEmpty(base64Key)) {
			key = new SecretKeySpec(Base64.decodeBase64(base64Key), ALGORITHM);
		} else {
			KeyGenerator keyGen = KeyGenerator.getInstance(ALGORITHM);
			keyGen.init(128);
			key = keyGen.generateKey();
		}
		
		String keyBase64 = Base64.encodeBase64String(key.getEncoded());
		this.p.setProperty("key", keyBase64);
    }
 
    /**
     * Decrypt a value in the Properties
     * @param key of the value
     * @return unencrypted value
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public String getDecryptedValue(String key) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
    	String base64EncodedValue = p.getProperty(key);
		if (StringUtils.isNotEmpty(base64EncodedValue)) {
			String decodedValue = decrypt(Base64.decodeBase64(base64EncodedValue));
			return decodedValue;
		}
		return null;
    }
    
    /**
     * @param value
     * @return
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public String encryptValue(String value) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		String encValueBase64  = Base64.encodeBase64String(encrypt(value));
		return encValueBase64;
    }

    /**
     * @param input
     * @return
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    private final byte[] encrypt(String input)
        throws InvalidKeyException, 
               BadPaddingException,
               IllegalBlockSizeException {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] inputBytes = input.getBytes();
        return cipher.doFinal(inputBytes);
    }

    /**
     * @param encryptionBytes
     * @return
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    private final String decrypt(byte[] encryptionBytes)
        throws InvalidKeyException, 
               BadPaddingException,
               IllegalBlockSizeException {
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] recoveredBytes = 
          cipher.doFinal(encryptionBytes);
        String recovered = 
          new String(recoveredBytes);
        return recovered;
    }
}
