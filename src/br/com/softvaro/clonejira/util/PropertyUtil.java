package br.com.softvaro.clonejira.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Class to load/write properties to/from file
 * @author colaborador
 *
 */
public class PropertyUtil {
	private static final String PROPERTY_FILE = ".clonejira.properties";
	private static final String USER_HOME = "user.home";
	
	public static final String KEY = "key";
	public static final String ORIGIN_SERVER = "servidorOrigem";
	public static final String ORIGIN_USER = "usuarioOrigem";		
	public static final String ORIGIN_PASSWORD = "passwordOrigem";
	public static final String TARGET_SERVER = "servidorDestino";
	public static final String TARGET_USER = "usuarioDestino";
	public static final String TARGET_PROJECT = "projetoDestino";
	public static final String TARGET_PASSWORD = "passwordDestino";
		
	/**
	 * Write properties into a file
	 * @param prop Properties object with properties to write into "<USER_HOME>\.clonejira.properties" file
	 */
	public static void writeProp(Properties prop){ 
		OutputStream output = null;

		try {
			String home = System.getProperty(USER_HOME);
			File propsFile = new File(home, PROPERTY_FILE);
			output = new FileOutputStream(propsFile);
			prop.store(output, null);
	
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
		}
	}
	
	/**
	 * Read properties from file
	 * @return Properties object with properties load from "<USER_HOME>\.clonejira.properties" file
	 */
	public static Properties getProperties() {
		try {
			String home = System.getProperty(USER_HOME);
			File propsFile = new File(home, PROPERTY_FILE);
			InputStream is;
			is = new FileInputStream(propsFile);
			Properties p = new java.util.Properties();
			p.load(is);
			return p;
		} catch (FileNotFoundException e) {
			System.out.println("getProperties properties file not found, creating an empty Properties object.");
			Properties p = new java.util.Properties();
			return p;
		} catch (IOException e) {
			System.out.println("getProperties properties file not found, creating an empty Properties object.");
			Properties p = new java.util.Properties();
			return p;
		}
	}
}
