package br.com.softvaro.clonejira.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.swing.JTextArea;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import br.com.softvaro.clonejira.EpicFieldsHdi;
import br.com.softvaro.clonejira.EpicFieldsSoftvaro;
import br.com.softvaro.clonejira.Fields;
import br.com.softvaro.clonejira.FieldsHdi;
import br.com.softvaro.clonejira.FieldsSoftvaro;

/**
 * Class to clone a Jira or a set of Jiras
 * 
 * @author emerson.takahashi
 *
 */
public class JiraCloner {

    // http://localhost:8080/rest/api/2/issue/createmeta
    // http://jira.softvaro.com.br/rest/api/2/issue
    private static final String HDI_SERVER = "http://hdixjira1:8080";
    private static final String SOFTVARO_SERVER = "http://jira.softvaro.com.br";

    private static final String SEARCH_URL_PATH = "/rest/api/2/search?";
    private static final String SEARCH_JQL_URL_PATH = SEARCH_URL_PATH + "jql=";
    private static final String ISSUE_URL_PATH = "/rest/api/2/issue";
    private static final Map<String, String> HDI_SOFTVARO_ISSUE_TYPE_MAP;
    private static final Map<String, String> HDI_SOFTVARO_STATUS_MAP;
    private static final Map<String, String> SOFTVARO_HDI_ISSUE_TYPE_MAP;
    private static final Map<String, String> SOFTVARO_HDI_STATUS_MAP;

    private static final String CAMPO_LINK_JIRA_EXTERNO_HDI = "customfield_10300";
    private static final String CAMPO_LINK_JIRA_EXTERNO_SOFTVARO = "customfield_10900";

    protected static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private JTextArea log;

    /**
     * Default constructor
     */
    public JiraCloner() {
	super();
    }

    /**
     * Constructor with logger as parameter
     */
    public JiraCloner(JTextArea log) {
	super();
	this.log = log;
    }

    // mappings from hdi to sofvaro
    static {
	Map<String, String> hdi2SoftvaroIssueTypeMap = new HashMap<String, String>();
	hdi2SoftvaroIssueTypeMap.put("10000", "6"); // epic
	hdi2SoftvaroIssueTypeMap.put("1", "9"); // hotfix <-> bug
	hdi2SoftvaroIssueTypeMap.put("1", "10100"); // sub-task bug <-> bug
	hdi2SoftvaroIssueTypeMap.put("10001", "7"); // story
	hdi2SoftvaroIssueTypeMap.put("3", "8"); // technical task
	hdi2SoftvaroIssueTypeMap.put("1", "1"); // bug
	hdi2SoftvaroIssueTypeMap.put("4", "4"); // improvement
	hdi2SoftvaroIssueTypeMap.put("3", "10101"); // divida <-> task
	hdi2SoftvaroIssueTypeMap.put("2", "2"); // new feature
	// hdi2SoftvaroIssueTypeMap.put("10001", "10000"); // specification <-> story
	hdi2SoftvaroIssueTypeMap.put("5", "5"); // sub-task
	hdi2SoftvaroIssueTypeMap.put("3", "3"); // task
	hdi2SoftvaroIssueTypeMap.put("10301", "10402"); // pendency
	hdi2SoftvaroIssueTypeMap.put("10100", "10404"); // duvida
	HDI_SOFTVARO_ISSUE_TYPE_MAP = (Map<String, String>) Collections.unmodifiableMap(hdi2SoftvaroIssueTypeMap);

	Map<String, String> softvaro2HdiIssueTypeMap = new HashMap<String, String>();
	softvaro2HdiIssueTypeMap.put("6", "10000"); // epic
	softvaro2HdiIssueTypeMap.put("9", "1"); // hotfix <-> bug
	softvaro2HdiIssueTypeMap.put("10100", "1"); // sub-task bug <-> bug
	softvaro2HdiIssueTypeMap.put("7", "10001"); // story
	softvaro2HdiIssueTypeMap.put("8", "3"); // technical task
	softvaro2HdiIssueTypeMap.put("1", "1"); // bug
	softvaro2HdiIssueTypeMap.put("4", "4"); // improvement
	softvaro2HdiIssueTypeMap.put("10101", "3"); // divida <-> task
	softvaro2HdiIssueTypeMap.put("2", "2"); // new feature
	// softvaro2HdiIssueTypeMap.put("10000", "10001"); // specification <-> story
	softvaro2HdiIssueTypeMap.put("5", "5"); // sub-task
	softvaro2HdiIssueTypeMap.put("3", "3"); // task
	softvaro2HdiIssueTypeMap.put("10402", "10301"); // pendency
	softvaro2HdiIssueTypeMap.put("10404", "10100"); // duvida

	SOFTVARO_HDI_ISSUE_TYPE_MAP = (Map<String, String>) Collections.unmodifiableMap(softvaro2HdiIssueTypeMap);

	Map<String, String> hdi2SoftvaroStateMap = new HashMap<String, String>();
	hdi2SoftvaroStateMap.put("10007", "1"); // backlog <-> para fazer
	hdi2SoftvaroStateMap.put("10100", "10006"); // em analise
	hdi2SoftvaroStateMap.put("10101", "3"); // em desenvolvimento
	hdi2SoftvaroStateMap.put("10103", "10007"); // em homologacao
	hdi2SoftvaroStateMap.put("10300", "10000"); // lib. pre-prod <-> revisao pre-prod
	hdi2SoftvaroStateMap.put("10301", "10000"); // em pre-prod <-> revisao pre-prod
	hdi2SoftvaroStateMap.put("10004", "10008"); // aguardando lib. para impl. prod
	hdi2SoftvaroStateMap.put("10105", "10000"); // lib. impl. prod
	hdi2SoftvaroStateMap.put("10106", "5"); // em prod <-> resolved pre-prod
	hdi2SoftvaroStateMap.put("6", "3"); // aceite prod
	HDI_SOFTVARO_STATUS_MAP = (Map<String, String>) Collections.unmodifiableMap(hdi2SoftvaroStateMap);

	Map<String, String> softvaro2HdiStateMap = new HashMap<String, String>();
	softvaro2HdiStateMap.put("1", "10007"); // backlog <-> para fazer (open)
	softvaro2HdiStateMap.put("4", "10007"); // backlog <-> para fazer (reopened)
	softvaro2HdiStateMap.put("10005", "10007"); // backlog <-> para fazer (em avaliacao softvaro)
	softvaro2HdiStateMap.put("10000", "10301"); // em pre-prod <-> revisao pre-prod
	softvaro2HdiStateMap.put("10001", "10006"); // em analise <-> em analise
	softvaro2HdiStateMap.put("10101", "10101"); // selected for development <-> em desenvolvimento
	softvaro2HdiStateMap.put("10006", "10100"); // em analise ti hdi
	softvaro2HdiStateMap.put("3", "10101"); // em desenvolvimento
	softvaro2HdiStateMap.put("10007", "10103"); // HDI lib. homo <-> em homologacao
	softvaro2HdiStateMap.put("10008", "10104"); // HDI fechado homo <-> aguardando lib. para impl. prod
	softvaro2HdiStateMap.put("10000", "10105"); // HDI lib. pre-prod <-> lib. impl. prod
	hdi2SoftvaroStateMap.put("5", "10106"); // em prod <-> resolved pre-prod
	softvaro2HdiStateMap.put("6", "3"); // HDI fechado pre-prod <-> aceite prod

	SOFTVARO_HDI_STATUS_MAP = (Map<String, String>) Collections.unmodifiableMap(softvaro2HdiStateMap);

    }

    /**
     * Method to clone a jira or a set of jiras
     * 
     * @param originServer
     *            server where do i get the original jira from
     * @param originUser
     *            user to connect to the originServer
     * @param originPwd
     *            password to be used in originServer
     * @param targetServer
     *            server where i will create the cloned jira
     * @param targetProject
     *            project in the targetServer where the cloned jira will be created
     * @param targetuser
     *            user to connect to the targetServer
     * @param targetPwd
     *            password to be used in targetServer
     * @param jiraKeys
     *            jira or a set of jiras to be cloned
     * @return
     */
    public String clone(String originServer, String originUser, String originPwd, String targetServer, String targetProject, String targetuser,
	    String targetPwd, String jiraKeys) {
	if (!validate(originServer, originUser, originPwd, targetServer, targetProject, targetuser, targetPwd, jiraKeys)) {
	    log("Processo de cloning finalizado.");
	    return null;
	}
	if (!verifyServers(originServer, targetServer)) {
	    log("Processo de cloning finalizado.");
	    return null;
	}
	StringTokenizer st = new StringTokenizer(jiraKeys, ",");
	StringBuffer stb = new StringBuffer();
	while (st.hasMoreElements()) {
	    String jiraKey = (String) st.nextElement();
	    jiraKey = StringUtils.trim(jiraKey);
	    log("Clonando " + jiraKey);
	    String clonedIssue = getCloned(originServer, originUser, originPwd, targetServer, targetProject, targetuser, targetPwd, jiraKey);
	    if (StringUtils.isNotEmpty(clonedIssue)) {
		log("Jira " + jiraKey + " ja esta clonado no Jira " + clonedIssue);
		continue; // issue is already cloned
	    }
	    String urlStr = originServer + SEARCH_JQL_URL_PATH + "key=" + jiraKey;
	    try {
		URL url = new URL(urlStr);
		System.out.println("clone conecting to " + urlStr);
		HttpURLConnection conn;
		conn = getConnection(url, originUser, originPwd);
		String jsonResponse = getIssueData(conn);
		System.out.println("clone received jsonResponse: " + jsonResponse);
		Fields issueFields = getFields(jsonResponse, originServer, targetServer, targetProject);
		if (issueFields == null) {
		    log("Jira " + jiraKey + " nao encontrado.");
		    continue;
		}
		// if (StringUtils.isNotEmpty(issueFields.getCustomfield_10006())) {
		// // the issue has a epic link, need to clone it
		// String epicLink = cloneEpic(originServer, originUser, originPwd,
		// targetServer, targetProject, targetuser, targetPwd,
		// issueFields.getCustomfield_10006());
		// issueFields.setCustomfield_10006(epicLink);
		// }
		// link para jira externo
		issueFields.setLinkJiraExterno(originServer + "/browse/" + jiraKey);
		Gson gson = new Gson();
		String jiraJsonObj = gson.toJson(issueFields);
		jiraJsonObj = jiraJsonObj.replaceAll("\\\\r", "\\\\\\r");
		jiraJsonObj = jiraJsonObj.replaceAll("\\\\t", "\\\\\\t");
		jiraJsonObj = jiraJsonObj.replaceAll("\\\\n", "\\\\\\n");
		System.out.println("clone json to create cloned issue: {\"fields\" :" + jiraJsonObj + "}");
		String createdIssue = createIssue("{\"fields\" :" + jiraJsonObj + "}", targetServer, targetuser, targetPwd);
		log("Jira " + jiraKey + " clonado no Jira " + createdIssue);

		stb.append(createdIssue + " ");

		// try to update origin issue, creating a link to the cloned issue
		try {
		    JSONObject jsonResponseIssueCreation = new JSONObject(createdIssue);
		    // try to get the issue key
		    String clonnedIssueKey = jsonResponseIssueCreation.getString("key");
		    String clonedLink = targetServer + "/browse/" + clonnedIssueKey;
		    // try to update it
		    updateExternaJiraLink(jiraKey, originServer, originUser, originPwd, clonedLink);
		} catch (JSONException e) {
		    log("Erro ao atualizar o Jira issue original com o link para o clone.");
		    System.out.println("clone error trying to update reference link in the original issue");
		    e.printStackTrace();
		}
	    } catch (IOException e) {
		log("Erro clonando o jira " + jiraKey);
		e.printStackTrace();
	    }
	}
	log("Processo de clonning finalizado.");
	return stb.toString();
    }

    /**
     * Clones an epic link
     * 
     * @param originServer
     * @param originUser
     * @param originPwd
     * @param targetServer
     * @param targetProject
     * @param targetuser
     * @param targetPwd
     * @param epicKey
     * @return
     */
    public String cloneEpic(String originServer, String originUser, String originPwd, String targetServer, String targetProject, String targetuser,
	    String targetPwd, String epicKey) {
	if (!validate(originServer, originUser, originPwd, targetServer, targetProject, targetuser, targetPwd, epicKey)) {
	    log("Processo de cloning do epico finalizado.");
	    return null;
	}
	String jiraKey = StringUtils.trim(epicKey);
	log("Clonando Epic: " + jiraKey);
	String clonedIssue = getCloned(originServer, originUser, originPwd, targetServer, targetProject, targetuser, targetPwd, jiraKey);
	if (StringUtils.isNotEmpty(clonedIssue)) {
	    log("Epic link " + jiraKey + " ja esta clonado no epic link " + clonedIssue);
	    return clonedIssue; // issue is already cloned
	}
	String urlStr = originServer + SEARCH_JQL_URL_PATH + "key=" + jiraKey;
	System.out.println("cloneEpci retrieving -> " + urlStr);
	try {
	    URL url = new URL(urlStr);
	    HttpURLConnection conn;
	    conn = getConnection(url, originUser, originPwd);
	    String jsonResponse = getIssueData(conn);
	    Fields issueFields = getEpicFields(jsonResponse, originServer, targetServer, targetProject);

	    // link para jira externo
	    issueFields.setLinkJiraExterno(originServer + "/browse/" + jiraKey);
	    Gson gson = new Gson();
	    String jiraJsonObj = gson.toJson(issueFields);
	    jiraJsonObj = jiraJsonObj.replaceAll("\\\\r", "\r");
	    jiraJsonObj = jiraJsonObj.replaceAll("\\\\t", "\t");
	    jiraJsonObj = jiraJsonObj.replaceAll("\\\\n", "\n");
	    System.out.println("cloneEpic json to create epic link: {\"fields\" :" + jiraJsonObj + "}");
	    String createdIssueKey = createIssue("{\"fields\" :" + jiraJsonObj + "}", targetServer, targetuser, targetPwd);
	    JSONObject jsonEpic = new JSONObject(createdIssueKey);
	    String newEpicKey = jsonEpic.getString("key");
	    log("Epic Link " + jiraKey + " clonado no Epic Link " + createdIssueKey);
	    return newEpicKey;
	} catch (Exception e) {
	    log("Erro clonando o epic link " + jiraKey + ". " + e.getMessage());
	    e.printStackTrace();
	}

	return null;
    }

    /**
     * Create a new issue on Jira
     * 
     * @param data
     * @return
     */
    private String createIssue(String data, String targetServer, String targetUser, String targetPassword) {
	System.out.println("createIssue data:" + data);
	String line;
	StringBuffer jsonString = new StringBuffer();
	HttpURLConnection connection = null;
	try {

	    URL url = new URL(targetServer + ISSUE_URL_PATH);

	    // String data = "{\"fields\" :{\"project\":{ \"key\": \"TIWS\"},\"summary\": \"Another Issue created using REST API\",\"description\":
	    // \"Creating of an issue using project keys and issue type names using the REST API\",\"issuetype\": {\"name\": \"Tarefa\"}}}";
	    connection = (HttpURLConnection) url.openConnection();
	    String authString = targetUser + ":" + targetPassword;
	    String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
	    connection.setRequestProperty("Authorization", "Basic " + authStringEnc);

	    connection.setDoInput(true);
	    connection.setDoOutput(true);
	    connection.setRequestMethod("POST");
	    connection.setRequestProperty("Accept", "application/json");
	    connection.setRequestProperty("Content-Type", "application/json; Charset=iso-8859-1");
	    connection.connect();
	    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "utf-8");
	    writer.write(data);
	    writer.close();
	    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	    while ((line = br.readLine()) != null) {
		jsonString.append(line);
	    }
	    br.close();
	    System.out.println("createIssue response code: " + connection.getResponseCode());
	    connection.disconnect();
	} catch (Exception e) {
	    log("Erro clonando jira: " + e.getMessage());
	    if (connection != null) {
		BufferedReader br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
		try {
		    while ((line = br.readLine()) != null) {
			log(line);
			System.err.println(line);
		    }
		    br.close();
		} catch (IOException e1) {
		    e1.printStackTrace();
		}
	    }
	    log("issue: " + data);
	    e.printStackTrace();
	    throw new RuntimeException(e.getMessage());
	}
	System.out.println("createIssue cloned jira key(s): " + jsonString.toString());
	return jsonString.toString();
    }

    /**
     * Update the field "Link para Jira externo"
     * 
     * @param data
     * @return
     */
    private String updateExternaJiraLink(String jiraKey, String targetServer, String targetUser, String targetPassword, String newJiraLink) {
	System.out.println("updateExternaJiraLink jiraKey: " + jiraKey + " newJiraLink: " + newJiraLink);
	String line;
	StringBuffer jsonString = new StringBuffer();
	HttpURLConnection connection = null;
	String data = null;
	try {

	    URL url = new URL(targetServer + ISSUE_URL_PATH + "/" + jiraKey);

	    if (HDI_SERVER.equals(targetServer)) {
		data = "{\"fields\" :{\"" + CAMPO_LINK_JIRA_EXTERNO_HDI + "\": \"" + newJiraLink + "\"}}}";
	    } else if (SOFTVARO_SERVER.equals(targetServer)) {
		data = "{\"fields\" :{\"" + CAMPO_LINK_JIRA_EXTERNO_SOFTVARO + "\": \"" + newJiraLink + "\"}}}";
	    }
	    System.out.println("updating data: " + data);
	    connection = (HttpURLConnection) url.openConnection();
	    String authString = targetUser + ":" + targetPassword;
	    String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
	    connection.setRequestProperty("Authorization", "Basic " + authStringEnc);

	    connection.setDoInput(true);
	    connection.setDoOutput(true);
	    connection.setRequestMethod("PUT");
	    connection.setRequestProperty("Accept", "application/json");
	    connection.setRequestProperty("Content-Type", "application/json; Charset=UTF-8");
	    connection.connect();
	    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
	    writer.write(data);
	    writer.close();
	    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	    while ((line = br.readLine()) != null) {
		jsonString.append(line);
	    }
	    br.close();
	    System.out.println("createIssue response code: " + connection.getResponseCode());
	    connection.disconnect();
	} catch (Exception e) {
	    log("Erro atualizando o jira: " + e.getMessage());
	    if (connection != null) {
		BufferedReader br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
		try {
		    while ((line = br.readLine()) != null) {
			log(line);
			System.err.println(line);
		    }
		    br.close();
		} catch (IOException e1) {
		    e1.printStackTrace();
		}
	    }
	    // log("issue: " + data);
	    e.printStackTrace();
	    throw new RuntimeException(e.getMessage());
	}
	System.out.println("createIssue cloned jira key(s): " + jsonString.toString());
	return jsonString.toString();
    }

    /**
     * Get a Fields object from a jira query response
     * 
     * @param jsonResponse
     * @param originServer
     * @param targetServer
     * @param targetProject
     * @return
     */
    private Fields getFields(String jsonResponse, String originServer, String targetServer, String targetProject) {
	System.out.println("getFields from " + jsonResponse);
	JSONObject obj = new JSONObject(jsonResponse);
	JSONArray arr = obj.getJSONArray("issues");
	if (arr.length() == 0) {
	    return null;
	}
	JSONObject issue = arr.getJSONObject(0);
	JSONObject fields = issue.getJSONObject("fields");
	JSONObject issueType = fields.getJSONObject("issuetype");
	// JSONObject status = fields.getJSONObject("status");
	JSONObject priority = fields.getJSONObject("priority");
	// JSONObject project = fields.getJSONObject("project");
	String key = issue.getString("key");

	Fields jiraIssue = null;
	if (HDI_SERVER.equals(targetServer)) {
	    jiraIssue = new FieldsHdi();
	    // try {
	    // jiraIssue.setCustomfield_10401(SDF.parse(fields.getString("customfield_11400")));
	    // } catch (JSONException e) {
	    // System.out.println("getFields error getting Deadline (customfield_10006)");
	    // // e.printStackTrace();
	    // } catch (ParseException e) {
	    // System.out.println("error parsing Deadline (" + fields.getString("customfield_11400") + ")");
	    // e.printStackTrace();
	    // }
	    //
	} else {
	    jiraIssue = new FieldsSoftvaro();
	    // try {
	    // jiraIssue.setCustomfield_11400(SDF.parse(fields.getString("customfield_10401")));
	    // } catch (JSONException e) {
	    // System.out.println("getFields error getting Deadline (customfield_10006)");
	    // // e.printStackTrace();
	    // } catch (ParseException e) {
	    // System.out.println("error parsing Deadline (" + fields.getString("customfield_10401") + ")");
	    // e.printStackTrace();
	    // }
	    //
	    // }
	    // epic link
	    // try {
	    // jiraIssue.setCustomfield_10006(fields.getString("customfield_10006"));
	    // } catch (JSONException e) {
	    // System.out.println("getFields error getting epic link (customfield_10006)");
	    // //e.printStackTrace();
	}
	try {
	    if (!fields.isNull("description")) {
		jiraIssue.setDescription(fields.getString("description"));
	    }
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	try {
	    if (!fields.isNull("summary")) {
		jiraIssue.setSummary(fields.getString("summary"));
	    }
	} catch (JSONException e) {
	    e.printStackTrace();
	}

	// I can't set the issue status when creating it
	// need to transition it to the desired status after creation
	// try {
	// jiraIssue.setStatus(jiraIssue.new Status(getStatus(status.getString("id"), originServer, targetServer)));
	// } catch (JSONException e) {
	// e.printStackTrace();
	// }
	try {
	    jiraIssue.setIssuetype(jiraIssue.new IssueType(getIssueType(issueType.getString("id"), originServer, targetServer)));
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	try {
	    jiraIssue.setPriority(jiraIssue.new Priority(priority.getString("id")));
	} catch (JSONException e) {
	    e.printStackTrace();
	}
	try {
	    jiraIssue.setProject(jiraIssue.new Project(targetProject));
	} catch (JSONException e) {
	    e.printStackTrace();
	}

	return jiraIssue;
    }

    /**
     * Get a Epic Fields object from a jira query response
     * 
     * @param jsonResponse
     * @param originServer
     * @param targetServer
     * @param targetProject
     * @return
     */
    private Fields getEpicFields(String jsonResponse, String originServer, String targetServer, String targetProject) {
	System.out.println("getEpicFields from: " + jsonResponse);
	JSONObject obj = new JSONObject(jsonResponse);
	JSONArray arr = obj.getJSONArray("issues");
	JSONObject issue = arr.getJSONObject(0);
	JSONObject fields = issue.getJSONObject("fields");
	JSONObject issueType = fields.getJSONObject("issuetype");
	// I can't set the status when creating an issue
	// JSONObject status = fields.getJSONObject("status");
	JSONObject priority = fields.getJSONObject("priority");

	Fields jiraIssue = null;
	if (HDI_SERVER.equals(targetServer)) {
	    jiraIssue = new EpicFieldsHdi();
	} else {
	    jiraIssue = new EpicFieldsSoftvaro();
	}
	// epic link
	try {
	    if (HDI_SERVER.equals(originServer)) {
		jiraIssue.setEpicName(fields.getString("customfield_10005"));
	    } else {
		jiraIssue.setEpicName(fields.getString("customfield_10007"));
	    }
	} catch (JSONException e) {
	    System.out.println("Issue doesn't have a epic link.");
	    // e.printStackTrace();
	}
	try {
	    jiraIssue.setSummary(fields.getString("summary"));
	} catch (JSONException e) {
	    System.out.println("Issue doesn't have a summary.");
	    // e.printStackTrace();
	}

	// I can't set the status when creating an issue
	// try {
	// jiraIssue.setStatus(jiraIssue.new Status(getStatus(status.getString("id"), originServer, targetServer)));
	// } catch (JSONException e) {
	// e.printStackTrace();
	// }

	try {
	    jiraIssue.setIssuetype(jiraIssue.new IssueType(getIssueType(issueType.getString("id"), originServer, targetServer)));
	} catch (JSONException e) {
	    System.out.println("Issue doesn't have an issue type. This is weird!!!");
	    // e.printStackTrace();
	}
	try {
	    jiraIssue.setPriority(jiraIssue.new Priority(priority.getString("id")));
	} catch (JSONException e) {
	    System.out.println("Issue doesn't have priority.");
	    // e.printStackTrace();
	}
	try {
	    jiraIssue.setProject(jiraIssue.new Project(targetProject));
	} catch (JSONException e) {
	    e.printStackTrace();
	}

	return jiraIssue;
    }

    /**
     * Need to find the equivalent issue type from the source to the target server
     * 
     * @param issueType
     * @param originServer
     * @param targetServer
     * @return
     */
    private String getIssueType(String issueType, String originServer, String targetServer) {
	System.out.println("getIssueType " + issueType + " origin " + originServer + " target " + targetServer);
	String ret = issueType;
	if (HDI_SERVER.equals(originServer)) {
	    if (SOFTVARO_SERVER.equals(targetServer)) {
		ret = HDI_SOFTVARO_ISSUE_TYPE_MAP.get(issueType);
	    }
	} else if (SOFTVARO_SERVER.equals(originServer)) {
	    if (HDI_SERVER.equals(targetServer)) {
		ret = SOFTVARO_HDI_ISSUE_TYPE_MAP.get(issueType);
	    }
	}
	System.out.println("getIssueType returning " + ret);
	return ret;
    }

    /**
     * Need to find the equivalent status from the source to the target server
     * 
     * @param status
     * @param originServer
     * @param targetServer
     * @return
     */
    private String getStatus(String status, String originServer, String targetServer) {
	String ret = status;
	if (HDI_SERVER.equals(originServer)) {
	    if (SOFTVARO_SERVER.equals(targetServer)) {
		ret = HDI_SOFTVARO_STATUS_MAP.get(status);
	    }
	} else if (SOFTVARO_SERVER.equals(originServer)) {
	    if (HDI_SERVER.equals(targetServer)) {
		ret = SOFTVARO_HDI_STATUS_MAP.get(status);
	    }
	}
	return ret;
    }

    /**
     * Validate parameters
     * 
     * @param originServer
     * @param originUser
     * @param originPwd
     * @param targetServer
     * @param targetProject
     * @param targetuser
     * @param targetPwd
     * @param jiraKeys
     * @return
     */
    public boolean validate(String originServer, String originUser, String originPwd, String targetServer, String targetProject, String targetuser,
	    String targetPwd, String jiraKeys) {
	if (StringUtils.isEmpty(jiraKeys)) {
	    log("Nenhum Jira issue-key informado.");
	    return false;
	}

	if (StringUtils.isEmpty(originServer)) {
	    log("Servidor origem não informado.");
	    return false;
	}
	if (StringUtils.isEmpty(originUser)) {
	    log("Usuário no servidor de origem não informado.");
	    return false;
	}
	if (StringUtils.isEmpty(originPwd)) {
	    log("Senha do usuário no servidor de origem não informada.");
	    return false;
	}
	if (StringUtils.isEmpty(targetServer)) {
	    log("Servidor destino não informado.");
	    return false;
	}
	if (StringUtils.isEmpty(targetProject)) {
	    log("Projeto no servidor destino não informado.");
	    return false;
	}
	if (StringUtils.isEmpty(targetuser)) {
	    log("Usuário no servidor destino não informado.");
	    return false;
	}
	if (StringUtils.isEmpty(targetPwd)) {
	    log("Senha do usuário no servidor destino não informada.");
	    return false;
	}
	if (StringUtils.isEmpty(originServer)) {
	    log("Servidor de origem nao informado.");
	    return false;
	}
	if (StringUtils.isEmpty(targetServer)) {
	    log("Servidor destino nao informado");
	    return false;
	}

	return true;
    }

    /**
     * Validate parameters
     * 
     * @param originServer
     * @param originUser
     * @param originPwd
     * @param targetServer
     * @param targetProject
     * @param targetuser
     * @param targetPwd
     * @param jiraKeys
     * @return
     */
    public boolean verifyServers(String originServer, String targetServer) {
	log("Verificando disponibilidade do servidor " + originServer);
	if (ping(originServer, 1000)) {
	    log("Servidor " + originServer + " ativo.");
	} else {
	    log("Servidor origem: " + originServer + " não encontrado.");
	    return false;
	}
	log("Verificando disponibilidade do servidor " + targetServer);
	if (ping(targetServer, 3000)) {
	    log("Servidor " + targetServer + " ativo.");
	} else {
	    log("Servidor origem: " + targetServer + " não encontrado.");
	    return false;
	}

	return true;
    }

    /**
     * Verify if a jira isn't already cloned
     * 
     * @param jira
     * @return
     */
    public String getCloned(String originServer, String originUser, String originPwd, String targetServer, String targetProject, String targetuser,
	    String targetPwd, String jira) {
	// an example of a valid search url in jira:
	// http://ubuntuserver:8090/rest/api/latest/search?project=TIWS&jql=cf[10200]="http://hdixjira1:8080/browse/TIS-16"&fields=key,created
	String urlStr = null;
	if (HDI_SERVER.equals(targetServer)) {
	    urlStr = targetServer + SEARCH_URL_PATH + "project=" + targetProject + "&jql=cf[10300]=\"" + originServer + "/browse/" + jira
		    + "\"&fields=key,created";
	} else if (SOFTVARO_SERVER.equals(targetServer)) {
	    urlStr = targetServer + SEARCH_URL_PATH + "project=" + targetProject + "&jql=cf[10900]=\"" + originServer + "/browse/" + jira
		    + "\"&fields=key,created";
	} else {
	    urlStr = targetServer + SEARCH_URL_PATH + "project=" + targetProject + "&jql=cf[10200]=\"" + originServer + "/browse/" + jira
		    + "\"&fields=key,created";
	}
	System.out.println("getCloned verifying if jira is cloned -> " + urlStr);
	try {
	    URL url = new URL(urlStr);
	    HttpURLConnection conn;
	    conn = getConnection(url, targetuser, targetPwd);
	    String jsonResponse = getIssueData(conn);
	    System.out.println("getCloned jsonResponse: " + jsonResponse);
	    JSONObject obj = new JSONObject(jsonResponse);
	    JSONArray arr = obj.getJSONArray("issues");
	    if (arr.length() > 0) {
		JSONObject issue = arr.getJSONObject(0);
		String key = issue.getString("key");
		if (StringUtils.isNotBlank(key)) {
		    return key;
		}
	    } else {
		return null;
	    }
	} catch (IOException e) {
	    log("Erro verificando se o jira ja esta clonado.");
	    e.printStackTrace();
	    return null;
	}

	return null;
    }

    /**
     * Given a connection, gets issue data
     * 
     * @param conn
     * @return
     */
    private String getIssueData(HttpURLConnection conn) {
	StringBuilder sb = new StringBuilder();
	try {
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("Accept", "application/json");

	    if (conn.getResponseCode() != 200) {
		log("Erro buscando dados do jira");
		log(conn.getResponseMessage());
		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());

	    }

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

	    String output;

	    System.out.println("getIssueData output from Server .... \n");
	    while ((output = br.readLine()) != null) {
		System.out.println(output);
		sb.append(output);
	    }

	    conn.disconnect();
	} catch (ProtocolException e) {
	    e.printStackTrace();
	    log("Erro recuperando dados do jira: " + e.getMessage());
	} catch (IOException e) {
	    e.printStackTrace();
	    log("Erro recuperando dados do jira: " + e.getMessage());
	}
	return sb.toString();
    }

    /**
     * @param url
     * @param username
     * @param password
     * @return
     * @throws IOException
     */
    private HttpURLConnection getConnection(URL url, String username, String password) throws IOException {
	HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
	String authString = username + ":" + password;
	String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
	urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
	return urlConnection;
    }

    /**
     * Pings a HTTP URL. This effectively sends a HEAD request and returns <code>true</code> if the response code is in the 200-399 range.
     * 
     * @param url
     *            The HTTP URL to be pinged.
     * @param timeout
     *            The timeout in millis for both the connection timeout and the response read timeout. Note that the total timeout is effectively two
     *            times the given timeout.
     * @return <code>true</code> if the given HTTP URL has returned response code 200-399 on a HEAD request within the given timeout, otherwise
     *         <code>false</code>.
     */
    public static boolean ping(String url, int timeout) {
	url = url.replaceFirst("^https", "http"); // Otherwise an exception may be thrown on invalid SSL certificates.

	try {
	    HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
	    connection.setConnectTimeout(timeout);
	    connection.setReadTimeout(timeout);
	    connection.setRequestMethod("HEAD");
	    int responseCode = connection.getResponseCode();
	    return (200 <= responseCode && responseCode <= 399);
	} catch (IOException exception) {
	    exception.printStackTrace();
	    return false;
	}
    }

    /**
     * @return the log
     */
    public JTextArea getLog() {
	return log;
    }

    /**
     * @param log
     *            the log to set
     */
    public void setLog(JTextArea log) {
	this.log = log;
    }

    /**
     * @param msg
     */
    private void log(String msg) {
	if (log != null) {
	    log.append(msg);
	    log.append("\n");
	} else {
	    System.out.println(msg);
	}
    }

}
