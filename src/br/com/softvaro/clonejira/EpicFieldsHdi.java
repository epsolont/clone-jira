package br.com.softvaro.clonejira;


/**
 * Class to represent data from a Jira issue (epic link)
 * @author colaborador
 *
 */
public class EpicFieldsHdi extends Fields {

	//epic name
	private String customfield_10005; 
	//link para jira externo
	private String customfield_10300; 

	public EpicFieldsHdi() {
		super();
	}

	@Override
	public void setEpicName(String epicName) {
		this.customfield_10005 = epicName;
	}
	
	@Override
	public void setLinkJiraExterno(String linkJiraExterno) {
		this.customfield_10300 = linkJiraExterno;
	}
	
	/**
	 * @return the customfield_10300
	 */
	public String getCustomfield_10300() {
		return customfield_10300;
	}

	/**
	 * @param customfield_10300 the customfield_10300 to set
	 */
	public void setCustomfield_10300(String customfield_10300) {
		this.customfield_10300 = customfield_10300;
	}

	
	/**
	 * @return the customfield_10005
	 */
	public String getCustomfield_10005() {
		return customfield_10005;
	}

	/**
	 * @param customfield_10005 the customfield_10005 to set
	 */
	public void setCustomfield_10005(String customfield_10005) {
		this.customfield_10005 = customfield_10005;
	}

}
