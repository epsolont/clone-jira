package br.com.softvaro.clonejira;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

public class ConnTest {
    private static final String SOURCE_SERVER_BASE_URL = "http://jira.softvaro.com.br/rest/api/latest/search?jql=key=";
    private static final String SOURCE_SERVER_USERNAME = "emerson.takahashi";
    private static final String SOURCE_SERVER_PASSWORD = "softv@4dminEYT";
    private static final String TARGET_SERVER_BASE_URL = "http://hdixjira1:8080/rest/api/latest/issue";
    private static final String TARGET_SERVER_USERNAME = "teste";
    private static final String TARGET_SERVER_PASSWORD = "softv@4dmin";
    private static final String TARGET_SERVER_PROJECT = "TIS";

    private final String baseUrl;
    private final String username;
    private final String password;

    public ConnTest() {
	super();
	this.baseUrl = "";
	this.username = "";
	this.password = "";
    }

    public ConnTest(String baseUrl, String username, String password) {
	this.baseUrl = baseUrl;
	this.username = username;
	this.password = password;
    }

    /**
     * @param args
     * @throws ClientProtocolException
     * @throws IOException
     */
    /**
     * @param args
     * @throws ClientProtocolException
     * @throws IOException
     */
    public static void main(String[] args) throws ClientProtocolException, IOException {
	// HttpClient client = HttpClientBuilder.create().build();
	// HttpGet request = new HttpGet("http://restUrl");
	// HttpResponse response = client.execute(request);
	// BufferedReader rd = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
	// String line = "";
	// while ((line = rd.readLine()) != null) {
	// System.out.println(line);
	// }
	// Test test = new Test("http://jira.softvaro.com.br/","emerson.takahashi", "softv@4dminEYT");
	// String jsonResponse = test.getDataFromServer("/rest/api/latest/search?jql=key=JIRASFTV-1071+order+by+created&maxResults=1");
	// URL url = new URL(SOURCE_SERVER_BASE_URL+"TIWS-16");
	URL url = new URL(SOURCE_SERVER_BASE_URL + "TES-2");
	ConnTest test = new ConnTest();
	HttpURLConnection conn = test.getConnection(url, SOURCE_SERVER_USERNAME, SOURCE_SERVER_PASSWORD);
	String jsonResponse = test.getIssueData(conn);
	// String jsonResponse = "{issues:[{}]}";
	System.out.println(jsonResponse);
	JSONObject obj = new JSONObject(jsonResponse);
	JSONArray arr = obj.getJSONArray("issues");
	System.out.println(arr.getJSONObject(0).toString());
	JSONObject issue = arr.getJSONObject(0);
	JSONObject fields = issue.getJSONObject("fields");
	JSONObject issueType = fields.getJSONObject("issuetype");
	JSONObject priority = fields.getJSONObject("priority");
	JSONObject project = fields.getJSONObject("project");
	String key = issue.getString("key");
	System.out.println("key-> " + key);
	Gson gson = new Gson();
	// MyJiraIssue issue = gson.fromJson(arr.getJSONObject(0).toString(),MyJiraIssue.class);
	// Map<String, String> map = gson.fromJson(fields.toString(), new TypeToken<Map<String, String>>() {}.getType());
	// String summary = map.get("summary");
	// MyJiraIssue fieldsIssue = gson.fromJson(fields.toString(),MyJiraIssue.class);
	System.out.println("issue-> " + issue);
	System.out.println("fields-> " + fields.toString());
	System.out.println("summary-> " + fields.getString("summary"));
	System.out.println("description-> " + fields.getString("description"));
	// System.out.println("description-> " + fields.getString("description"));

	FieldsHdi jiraIssue = new FieldsHdi();
	// jiraIssue.setKey(key);
	// jiraIssue.setCustomfield_10006(fields.getString("customfield_10006"));
	jiraIssue.setDescription(fields.getString("description"));
	jiraIssue.setSummary(fields.getString("summary"));
	// jiraIssue.setIssuetype(jiraIssue.new IssueType(issueType.getString("id")));
	jiraIssue.setIssuetype(jiraIssue.new IssueType("10100"));
	jiraIssue.setPriority(jiraIssue.new Priority(priority.getString("id")));
	// jiraIssue.setProject(jiraIssue.new Project(project.getString("key")));
	jiraIssue.setProject(jiraIssue.new Project(TARGET_SERVER_PROJECT));
	// jiraIssue.setCustomfield_10200("http://jira.softvaro.com.br/browse/"+"JIRASFTV-1071");
	String jiraJsonObj = gson.toJson(jiraIssue);
	// jiraJsonObj = jiraJsonObj.replace("\"","\\\"");
	jiraJsonObj = jiraJsonObj.replaceAll("\\\\r", "\\\\\\r");
	jiraJsonObj = jiraJsonObj.replaceAll("\\\\t", "\\\\\\t");
	jiraJsonObj = jiraJsonObj.replaceAll("\\\\n", "\\\\\\n");
	// System.out.println("My jiraJsonObj -> {\\\"fields\\\" :" + jiraJsonObj + "}");

	// \"customfield_10200\":\"http://ubuntuserver:8090/browse/TIWS-9\",

	// URL targetURL = new URL(TARGET_SERVER_BASE_URL);
	// HttpURLConnection targetConn = test.getConnection(targetURL, TARGET_SERVER_USERNAME, TARGET_SERVER_PASSWORD);
	// String data = "{\"fields\" :{\"description\":\"Alinhar com TI quando passarão chamar os serviços de limpezas da Home ajustados para o
	// padrão usado pelo Renato , já incluindo as limpezas da atualização cadastral, confpolitica e atribpolitica.\\r\\n\\r\\nVerificar qual capa
	// de lote vamos trabalhar\",\"summary\":\"Agendar atualização do Cache Manager em produção com limpezas da Home via trigger -
	// Renato\",\"customfield_10006\":\"\",\"issuetype\":{\"id\":\"3\"},\"priority\":{\"id\":\"2\"},\"project\":{\"key\":\"TIWS\"}}}";
	// String data = "{\"fields\" :{\"description\":\"Creating of an issue using project keys and issue type names using the REST
	// API\",\"summary\":\"SISCAS-12345 Issue created using REST
	// API\",\"status\":{\"id\":\"10101\"},\"issuetype\":{\"id\":\"10100\"},\"priority\":{\"id\":\"3\"},\"project\":{\"key\":\"CLON\"},\"customfield_10200\":\"http://ubuntuserver:8090/browse/TIWS-9\"}}";
	String data = "{\"fields\" :{\"description\":\"Creating of an issue using project keys and issue type names using the REST API\",\"summary\":\"SISCAS-12345 Issue created using REST API\",\"issuetype\":{\"id\":\"10100\"},\"priority\":{\"id\":\"2\"},\"project\":{\"key\":\"CLON\"}}}";
	test.createIssue("{\"fields\" :" + jiraJsonObj + "}");
	// test.createIssue(data);
	// String error = "{\"errorMessages\":[],\"errors\":{\"issuetype\":\"The issue type selected is invalid.\"}}";
	// JSONObject errorJson = new JSONObject(error);
	// System.out.println("errors: " + errorJson.getString("errors"));
    }

    /*
     * JSONObject obj = new JSONObject(" .... "); String pageName = obj.getJSONObject("pageInfo").getString("pageName");
     * 
     * JSONArray arr = obj.getJSONArray("posts"); for (int i = 0; i < arr.length(); i++) { String post_id = arr.getJSONObject(i).getString("post_id");
     * ...... }
     * 
     * 
     * "fields":{ "project":{ "key":"TIWS" }, "summary":"Issue created using REST API",
     * "description":"Creating of an issue using project keys and issue type names using the REST API", "issuetype":{ "name":"Tarefa" } }
     * 
     * 
     * 
     * curl -D- -H "Authorization: Basic amVua2luczpzb2Z0dkA0ZG1pbg==" -H "Content-Type: application/json" -X POST --data
     * "{\"fields\": {\"project\":{ \"key\": \"TIWS\"},\"summary\": \"Issue created using REST API\",\"description\": \"Creating of an issue using project keys and issue type names using the REST API\",\"issuetype\": {\"name\": \"Tarefa\"}}}"
     * http://ubuntuserver:8090/rest/api/latest/issue/
     */
    String getDataFromServer(String path) {
	StringBuilder sb = new StringBuilder();
	try {
	    URL url = new URL(baseUrl + path);
	    URLConnection urlConnection = setUsernamePassword(url);
	    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
	    String line;
	    while ((line = reader.readLine()) != null) {
		sb.append(line);
	    }
	    reader.close();

	    return sb.toString();
	} catch (Exception e) {
	    throw new RuntimeException(e);
	}
    }

    private URLConnection setUsernamePassword(URL url) throws IOException {
	// URLConnection urlConnection = url.openConnection();
	HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
	String authString = username + ":" + password;
	String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
	urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
	return urlConnection;
    }

    private HttpURLConnection getConnection(URL url, String username, String password) throws IOException {
	HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
	String authString = username + ":" + password;
	String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
	urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
	return urlConnection;
    }

    private String getIssueData(HttpURLConnection conn) {
	StringBuilder sb = new StringBuilder();
	try {
	    conn.setRequestMethod("GET");
	    conn.setRequestProperty("Accept", "application/json");

	    if (conn.getResponseCode() != 200) {
		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
	    }

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

	    String output;

	    System.out.println("Output from Server .... \n");
	    while ((output = br.readLine()) != null) {
		System.out.println(output);
		sb.append(output);
	    }

	    conn.disconnect();
	} catch (ProtocolException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return sb.toString();
    }

    private String createIssue(String data) {
	System.err.println("data->" + data);
	String line;
	StringBuffer jsonString = new StringBuffer();
	HttpURLConnection connection = null;
	try {

	    // data = "{\"fields\" :{\"description\":\"Alinhar com TI quando passarão chamar os serviços de limpezas da Home ajustados para o padrão
	    // usado pelo Renato , já incluindo as limpezas da atualização cadastral, confpolitica e atribpolitica.\\r\\n\\r\\nVerificar qual capa de
	    // lote vamos trabalhar\",\"summary\":\"Agendar atualização do Cache Manager em produção com limpezas da Home via trigger -
	    // Renato\",\"customfield_10006\":\"\",\"issuetype\":{\"id\":\"10100\"},\"priority\":{\"id\":\"2\"},\"project\":{\"key\":\"TIWS\"}}}";
	    // System.err.println("data2->" + data);
	    URL url = new URL(TARGET_SERVER_BASE_URL);

	    // String data = "{\"fields\" :{\"description\":\"Alinhar com TI quando passarão chamar os serviços de limpezas da Home ajustados para o
	    // padrão usado pelo Renato , já incluindo as limpezas da atualização cadastral, confpolitica e atribpolitica.\r\n\r\nVerificar qual capa
	    // de lote vamos trabalhar\",\"summary\":\"Agendar atualização do Cache Manager em produção com limpezas da Home via trigger -
	    // Renato\",\"customfield_10006\":\"HDIDC-3664\",\"issuetype\":{\"id\":\"2\"},\"priority\":{\"id\":\"2\"},\"project\":{\"key\":\"TIWS\"}}}";
	    // \"description\":\"Alinhar com TI quando passarao chamar os serviços de limpezas da Home ajustados para o padrao usado pelo Renato , ja
	    // incluindo as limpezas da atualizacao cadastral, confpolitica e atribpolitica.\r\n\r\nVerificar qual capa de lote vamos trabalhar\",
	    // ,\"customfield_10006\":\"HDIDC-3664\",\"issuetype\":{\"id\":\"2\"},\"priority\":{\"id\":\"2\"} // \\r\\n\\r\\n
	    // String data = "{\"fields\" :{\"project\":{\"key\":\"TIWS\"}, \"description\":\"Alinhar com TI quando passarão chamar os serviços de
	    // limpezas da Home ajustados para o padrão usado pelo Renato , já incluindo as limpezas da atualização cadastral, confpolitica e
	    // atribpolitica.\\r\\n\\r\\nVerificar qual capa de lote vamos trabalhar\",\"summary\":\"Agendar atualização do Cache Manager em produção
	    // com limpezas da Home via trigger - Renato\",\"issuetype\": {\"id\": \"10100\"},\"priority\":{\"id\":\"2\"}}}";
	    // String data = "{\"fields\" :{\"description\":\"Alinhar com TI quando passarão chamar os serviços de limpezas da Home ajustados para o
	    // padrão usado pelo Renato , já incluindo as limpezas da atualização cadastral, confpolitica e atribpolitica.\\r\\n\\r\\nVerificar qual
	    // capa de lote vamos trabalhar\",\"summary\":\"Agendar atualização do Cache Manager em produção com limpezas da Home via trigger -
	    // Renato\",\"customfield_10006\":\"\",\"issuetype\":{\"id\":\"10100\"},\"priority\":{\"id\":\"2\"},\"project\":{\"key\":\"TIWS\"}}}";
	    // String data = "{\"fields\" :{\"project\":{ \"key\": \"TIWS\"},\"summary\": \"Another Issue created using REST API\",\"description\":
	    // \"Creating of an issue using project keys and issue type names using the REST API\",\"issuetype\": {\"name\": \"Tarefa\"}}}";
	    connection = (HttpURLConnection) url.openConnection();
	    String authString = TARGET_SERVER_USERNAME + ":" + TARGET_SERVER_PASSWORD;
	    String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
	    connection.setRequestProperty("Authorization", "Basic " + authStringEnc);

	    connection.setDoInput(true);
	    connection.setDoOutput(true);
	    connection.setRequestMethod("POST");
	    connection.setRequestProperty("Accept", "application/json");
	    connection.setRequestProperty("Content-Type", "application/json; Charset=UTF-8");
	    connection.connect();
	    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
	    writer.write(data);
	    writer.close();
	    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	    while ((line = br.readLine()) != null) {
		jsonString.append(line);
	    }
	    br.close();
	    System.out.println("response code: " + connection.getResponseCode());
	    System.out.println("response msg: " + connection.getResponseMessage());
	    connection.disconnect();
	} catch (Exception e) {
	    BufferedReader br2 = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
	    try {
		while ((line = br2.readLine()) != null) {
		    System.err.println(line);
		}
		br2.close();
	    } catch (IOException e1) {
		e1.printStackTrace();
	    }

	    try {
		System.out.println("response content: " + connection.getContent());
		System.out.println("response code: " + connection.getResponseCode());
		System.out.println("response msg: " + connection.getResponseMessage());
		BufferedReader br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
		while ((line = br.readLine()) != null) {
		    jsonString.append(line);
		}
		br.close();
	    } catch (IOException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	    }
	    e.printStackTrace();
	    throw new RuntimeException(e.getMessage());
	} finally {

	}
	System.out.println(jsonString.toString());
	return jsonString.toString();
    }

    private String testeCreateIssue() {
	StringBuilder sb = new StringBuilder();
	try {
	    URL url = new URL(TARGET_SERVER_BASE_URL);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    String authString = TARGET_SERVER_USERNAME + ":" + TARGET_SERVER_PASSWORD;
	    String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
	    conn.setRequestProperty("Authorization", "Basic " + authStringEnc);
	    conn.setDoOutput(true);
	    conn.setRequestMethod("POST");
	    conn.setAllowUserInteraction(false);
	    conn.setRequestProperty("Content-Type", "application/json");

	    String data = "{\"fields\" :{\"description\":\"Alinhar com TI quando passarão chamar os serviços de limpezas da Home ajustados para o padrão usado pelo Renato , já incluindo as limpezas da atualização cadastral, confpolitica e atribpolitica.\r\n\r\nVerificar qual capa de lote vamos trabalhar\",\"summary\":\"Agendar atualização do Cache Manager em produção com limpezas da Home via trigger - Renato\",\"customfield_10006\":\"HDIDC-3664\",\"issuetype\":{\"id\":\"2\"},\"priority\":{\"id\":\"2\"},\"project\":{\"key\":\"TIWS\"}}}";

	    // send query
	    PrintStream ps = new PrintStream(conn.getOutputStream());
	    ps.print(data);
	    ps.close();

	    if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
		conn.disconnect();
		throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
	    }

	    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

	    String output;
	    System.out.println("Output from Server .... \n");
	    while ((output = br.readLine()) != null) {
		System.out.println(output);
		sb.append(output);
	    }
	    br.close();

	    conn.disconnect();
	} catch (ProtocolException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return sb.toString();
    }

    // String username = ...
    // String password = ...
    // UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);
    //
    // HttpRequest request = ...
    // request.addHeader(new BasicScheme().authenticate(creds, request));

}
