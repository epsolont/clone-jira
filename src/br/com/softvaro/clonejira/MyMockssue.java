package br.com.softvaro.clonejira;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

//import com.atlassian.jira.bc.project.component.ProjectComponent;
//import com.atlassian.jira.issue.Issue;
//import com.atlassian.jira.issue.IssueKey;
//import com.atlassian.jira.issue.ModifiedValue;
//import com.atlassian.jira.issue.fields.CustomField;
//import com.atlassian.jira.issue.fields.renderer.IssueRenderContext;
//import com.atlassian.jira.issue.issuetype.IssueType;
//import com.atlassian.jira.issue.label.Label;
//import com.atlassian.jira.issue.priority.Priority;
//import com.atlassian.jira.issue.resolution.Resolution;
//import com.atlassian.jira.issue.status.Status;
//import com.atlassian.jira.project.Project;
//import com.atlassian.jira.user.ApplicationUser;
//import com.atlassian.jira.user.ApplicationUsers;







public class MyMockssue
{
  private Long id;
  private Long projectId;
  private String key;
  private String issueTypeId;
  private String summary;
  private String description;
  private String environment;
  private String reporterId;
  private String creatorId;
  private Timestamp created;
  private Timestamp updated;
  private Timestamp dueDate;
  private Long securityLevelId;
  private String priorityId;
  private String resolutionId;
  private String statusId;
  private Long votes;
  private Long watches;
  private Long originalEstimate;
  private Long estimate;
  private Long workflowId;
  private Long parentId;
  //private GenericValue genericValue;
//  private Set<Label> labels = new LinkedHashSet();
//  
//  private Map<String, ModifiedValue> modifiedFields;
  //private GenericValue project;
//  private IssueType issueType;
//  private Resolution resolution;
//  private IssueType issueTypeObject;
//  private ApplicationUser assignee;
//  private Collection<ProjectComponent> components;
//  private ApplicationUser reporter;
//  private ApplicationUser creator;
//  private Collection affectedVersions;
//  private Resolution resolutionObject;
//  private Collection fixVersions;
//  //private GenericValue securityLevel;
//  private Priority priority;
//  private Status status;
  private Long timeSpent;
//  private Status statusObject;
//  private Collection subTaskObjects = new ArrayList();
  private Timestamp resolutionDate;
  private boolean stored;
//  private Project projectObject;
  private Map<String, Object> externalFields;
  private Long number;
//  private Issue parentObject;
//  private boolean parentObjectSet = false;
  
  public MyMockssue() {
//    modifiedFields = new HashMap();
    externalFields = new HashMap();
    long now = System.currentTimeMillis();
    created = new Timestamp(now);
    updated = new Timestamp(now);
    dueDate = new Timestamp(now);
    resolutionDate = new Timestamp(now);
  }
  



  public MyMockssue(Long id)
  {
    this(id, null);
  }
  



  public MyMockssue(long id)
  {
    this(new Long(id), null);
  }
  



  public MyMockssue(int id, String key)
  {
    this(new Long(id), null);
    //setKey(key);
  }
  


  public MyMockssue(Long id, Long now)
  {
    this.id = id;
    if (now != null) {
      created = new Timestamp(now.longValue());
      updated = new Timestamp(now.longValue());
      dueDate = new Timestamp(now.longValue());
    }
//    modifiedFields = new HashMap();
    externalFields = new HashMap();
  }
  


//  public MyJiraIssue(GenericValue gv)
//  {
//    setGenericValue(gv);
//  }
  
  public Long getId() {
    return id;
  }
  
//  public GenericValue getProject() {
//    return null;
//  }
//  
//  public Project getProjectObject() {
//    if ((projectObject == null) && (projectId != null)) {
//      //return new ProjectImpl(new MockGenericValue("project", projectId));
//    return null;
//    }
//    return projectObject;
//  }
//  
//  public void setProjectObject(Project projectObject) {
//    this.projectObject = projectObject;
//    if (projectObject != null) {
//      project = projectObject.getGenericValue();
//      projectId = projectObject.getId();
//    } else {
//      project = null;
//      projectId = null;
//    }
//  }
  
//  public void setProject(GenericValue project) {
//    this.project = project;
//    projectObject = (project != null ? new ProjectImpl(project) : null);
//  }
//  
//  public IssueType getIssueType() {
//    return issueType;
//  }
//  
//  public IssueType getIssueTypeObject() {
//    return issueTypeObject;
//  }
//  
//  public void setIssueType(GenericValue issueType) {
//    setIssueType(new IssueTypeImpl(issueType, null, null, null, null));
//  }
//  
//  public void setIssueType(IssueType issueType) {
//    this.issueType = issueType;
//  }
  
  public void setIssueTypeId(String issueTypeId) {
    this.issueTypeId = issueTypeId;
  }
  
  public String getSummary() {
    return summary;
  }
  
  public void setSummary(String summary) {
    this.summary = summary;
  }
  
//  public ApplicationUser getAssigneeUser() {
//    return assignee;
//  }
//  
//  public ApplicationUser getAssignee() {
//    return assignee;
//  }
//  
//  public String getAssigneeId() {
//    return assignee != null ? assignee.getKey() : null;
//  }
//  
//  public void setAssignee(ApplicationUser assignee) {
//    this.assignee = assignee;
//  }
//  
//  public void setComponent(Collection<ProjectComponent> components)
//  {
//    if (components == null) {
//      components = Collections.emptyList();
//    }
//    
//    List<GenericValue> gvList = new ArrayList(components.size());
//    for (ProjectComponent component : components) {
//      gvList.add(component.getGenericValue());
//    }
//    
//    this.components = components;
//  }
//  
//  public Collection<ProjectComponent> getComponents() {
//    return components;
//  }
//  
//
//  public Collection<ProjectComponent> getComponentObjects()
//  {
//    return components;
//  }
//  
//  public void setAssigneeId(String assigneeId) {
//    assignee = ApplicationUsers.byKey(assigneeId);
//    if ((assignee == null) && (assigneeId != null)) {
//      String msg = String.format("Cannot find user for a key '%s'. Please mock UserManager#getUserByKey(\"%s\")", new Object[] { assigneeId, assigneeId });
//      throw new NullPointerException(msg);
//    }
//  }
//  
//  public ApplicationUser getReporterUser() {
//    return reporter;
//  }
//  
//  public ApplicationUser getReporter() {
//    return reporter;
//  }
  
  public String getReporterId() {
    return reporterId;
  }
  
//  public ApplicationUser getCreator()
//  {
//    return creator;
//  }
//  
//  public String getCreatorId()
//  {
//    String key = ApplicationUsers.getKeyFor(creator);
//    if ((key == null) && (creator != null)) {
//      String msg = String.format("Cannot find key for creator. Please mock UserKeyService#getKeyForUsername(\"%s\")", new Object[] { creator.getName() });
//      throw new NullPointerException(msg);
//    }
//    return key;
//  }
//  
//  public void setCreatorId(String creatorId) {
//    creator = ApplicationUsers.byKey(creatorId);
//    if ((creator == null) && (creatorId != null)) {
//      String msg = String.format("Cannot find user for a key '%s'. Please mock UserManager#getUserByKey(\"%s\")", new Object[] { creatorId, creatorId });
//      throw new NullPointerException(msg);
//    }
//  }
//  
//  public void setReporter(ApplicationUser reporter) {
//    this.reporter = reporter;
//  }
  
  public void setReporterId(String reporterId) {
    this.reporterId = reporterId;
  }
  
  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public String getEnvironment() {
    return environment;
  }
  
  public void setEnvironment(String environment) {
    this.environment = environment;
  }
  
//  public Collection getAffectedVersions() {
//    return affectedVersions;
//  }
//  
//  public Collection getFixVersions() {
//    return fixVersions;
//  }
  
  public Timestamp getDueDate() {
    return dueDate;
  }
  
//  public GenericValue getSecurityLevel() {
//    return null;
//  }
  
  public String getIssueTypeId() {
    return issueTypeId;
  }
  
  public String getPriorityId() {
    return priorityId;
  }
  
  public Long getProjectId() {
    return projectId;
  }
  
  public String getResolutionId() {
    return resolutionId;
  }
  
  public Long getSecurityLevelId() {
    return securityLevelId;
  }
  
//  public Priority getPriority() {
//    return null;
//  }
//  
//  public Priority getPriorityObject() {
//    return null;
//  }
  
  public String getStatusId() {
    return statusId;
  }
  
//  public void setAffectedVersions(Collection affectedVersions) {
//    this.affectedVersions = affectedVersions;
//  }
//  
//  public void setFixVersions(Collection fixVersions) {
//    this.fixVersions = fixVersions;
//  }
  
  public void setDueDate(Timestamp dueDate) {
    this.dueDate = dueDate;
  }
  
//  public void setSecurityLevel(GenericValue securityLevel) {
//    this.securityLevel = securityLevel;
//  }
  
//  public void setPriority(Priority priority) {
//    this.priority = priority;
//  }
//  
//  public void setPriorityObject(Priority priority)
//  {
//    setPriority(priority);
//  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
//  public void setIssueTypeObject(IssueType issueTypeObject) {
//    this.issueTypeObject = issueTypeObject;
//  }
  
  public void setProjectId(Long projectId) {
    this.projectId = projectId;
  }
  
  public void setSecurityLevelId(Long securityLevelId) {
    this.securityLevelId = securityLevelId;
  }
  
  public void setPriorityId(String priorityId)
  {
    this.priorityId = priorityId;
  }
  
//  public Resolution getResolution() {
//    return resolution;
//  }
//  
//  public void setResolutionObject(Resolution resolutionObject) {
//    this.resolutionObject = resolutionObject;
//  }
//  
//  public Resolution getResolutionObject() {
//    return resolutionObject;
//  }
//  
//  public void setResolution(GenericValue resolution) {
//    this.resolution = new ResolutionImpl(resolution, null, null, null);
//  }
//  
//  public void setResolution(Resolution resolution)
//  {
//    this.resolution = resolution;
//  }
  
  public String getKey() {
    return key;
  }
  
//  public void setKey(String key) {
//    this.key = key;
//    if (key == null) {
//      number = null;
//    } else {
//      IssueKey issueKey = IssueKey.from(key);
//      number = Long.valueOf(issueKey.getIssueNumber());
//    }
//  }
  
  public void setNumber(Long number)
  {
    this.number = number;
  }
  
  public Long getNumber()
  {
    return number;
  }
  
  public Long getVotes() {
    return votes;
  }
  
  public void setVotes(Long votes) {
    this.votes = votes;
  }
  
  public Long getWatches() {
    return watches;
  }
  
  public void setWatches(Long watches) {
    this.watches = watches;
  }
  
  public Timestamp getCreated() {
    return created;
  }
  
  public void setCreated(Timestamp created) {
    this.created = created;
  }
  
  public Timestamp getUpdated() {
    return updated;
  }
  
  public Timestamp getResolutionDate() {
    return resolutionDate;
  }
  
  public void setResolutionDate(Timestamp resolutionDate) {
    this.resolutionDate = resolutionDate;
  }
  
  public void setUpdated(Timestamp updated) {
    this.updated = updated;
  }
  
  public Long getWorkflowId() {
    return workflowId;
  }
  
//  public Object getCustomFieldValue(CustomField customField) {
//    return null;
//  }
//  
//  public Status getStatus() {
//    return status;
//  }
//  
//  public Status getStatusObject() {
//    return statusObject;
//  }
  
  public void setWorkflowId(Long workflowId) {
    this.workflowId = workflowId;
  }
  
  public void setStatusId(String statusId) {
    this.statusId = statusId;
  }
  
//  public Map<String, ModifiedValue> getModifiedFields() {
//    return modifiedFields;
//  }
//  
//  public void setParentObject(Issue parentIssue) {
//    parentObject = parentIssue;
//    parentObjectSet = true;
//  }
//  
//  public void setModifiedFields(Map modifiedFields) {
//    this.modifiedFields = modifiedFields;
//  }
//  
//  public void resetModifiedFields()
//  {
//    modifiedFields.clear();
//  }
  
  public boolean isSubTask() {
    return false;
  }
  
  public Long getParentId() {
    return parentId;
  }
  
  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }
  
  public boolean isCreated() {
    throw new UnsupportedOperationException("Not implemented.");
  }
  
//  public GenericValue getParent() {
//    throw new UnsupportedOperationException("Not implemented.");
//  }
  
//  public Issue getParentObject() {
//    if (parentObjectSet) {
//      return parentObject;
//    }
//    return new MyJiraIssue(getParentId());
//  }
  
//  public Collection getSubTasks()
//  {
//    throw new UnsupportedOperationException("Not implemented.");
//  }
//  
//  public Collection getSubTaskObjects() {
//    return subTaskObjects;
//  }
  
  public String getString(String name) {
    throw new UnsupportedOperationException("Not implemented.");
  }
  
  public Timestamp getTimestamp(String name) {
    throw new UnsupportedOperationException("Not implemented.");
  }
  
  public Long getLong(String name) {
    throw new UnsupportedOperationException("Not implemented.");
  }
  
//  public GenericValue getGenericValue() {
//    if (genericValue == null) {
//      return getHackedGVThatReturnsId();
//    }
//    return genericValue;
//  }
//  
//  protected GenericValue getHackedGVThatReturnsId() {
    //MockGenericValue gv = new MockGenericValue("Issue");
    //gv.set("id", getId());
    //gv.set("issuekey", getId());
    //gv.set("updated", getUpdated());
    //if (getSecurityLevelId() != null) {
    //  gv.set("security", getSecurityLevelId());
    //}
    //return gv;
//    return null;
//  }
//  
//  public void setGenericValue(GenericValue genericValue) {
//    this.genericValue = genericValue;
//    
//
//    setId(genericValue.getLong("id"));
//    setKey(genericValue.getString("key"));
//    setProjectId(genericValue.getLong("project"));
//    setSummary(genericValue.getString("summary"));
//    setDescription(genericValue.getString("description"));
//    setOriginalEstimate(genericValue.getLong("timeoriginalestimate"));
//    setEstimate(genericValue.getLong("timeestimate"));
//    setTimeSpent(genericValue.getLong("timespent"));
//    setUpdated(genericValue.getTimestamp("updated"));
//    setIssueTypeId(genericValue.getString("type"));
//    setEnvironment(genericValue.getString("environment"));
//    setAssigneeId(genericValue.getString("assignee"));
//    setReporterId(genericValue.getString("reporter"));
//    setCreatorId(genericValue.getString("creator"));
//    setDueDate(genericValue.getTimestamp("duedate"));
//    setSecurityLevelId(genericValue.getLong("security"));
//    setPriorityId(genericValue.getString("priority"));
//    setStatusId(genericValue.getString("status"));
//    setResolutionId(genericValue.getString("resolution"));
//    setCreated(genericValue.getTimestamp("created"));
//    setResolutionDate(genericValue.getTimestamp("resolutiondate"));
//    setVotes(genericValue.getLong("votes"));
//    setWatches(genericValue.getLong("watches"));
//    setWorkflowId(genericValue.getLong("workflowId"));
//  }
  
  public void store() {
    stored = true;
  }
  
  public boolean isStored() {
    return stored;
  }
  
  public void setResolutionId(String resolutionId) {
    this.resolutionId = resolutionId;
  }
  
  public boolean isEditable() {
    return true;
  }
  
//  public IssueRenderContext getIssueRenderContext() {
//    throw new UnsupportedOperationException("Not implemented");
//  }
//  
//  public Collection getAttachments() {
//    throw new UnsupportedOperationException("Not implemented");
//  }
//  
//  public void setCustomFieldValue(CustomField customField, Object value) {}
//  
//  public void setExternalFieldValue(String fieldId, Object value)
//  {
//    setExternalFieldValue(fieldId, null, value);
//  }
//  
//  public void setExternalFieldValue(String fieldId, Object oldValue, Object newValue) {
//    ModifiedValue modifiedValue = new ModifiedValue(oldValue, newValue);
//    externalFields.put(fieldId, newValue);
//    modifiedFields.put(fieldId, modifiedValue);
//  }
//  
//  public void setStatus(GenericValue status) {
//    this.status = new StatusImpl(status, null, null, null, null);
//  }
//  
//  public void setStatus(Status status)
//  {
//    this.status = status;
//  }
//  
//  public void setStatusObject(Status status)
//  {
//    this.status = status;
//  }
  
  public void setTimeSpent(Long timespent) {
    timeSpent = timespent;
  }
  
  public Long getEstimate() {
    return estimate;
  }
  
  public Long getTimeSpent() {
    return timeSpent;
  }
  
//  public void setLabels(Set<Label> labels) {
//    this.labels = new LinkedHashSet(labels);
//  }
//  
//  public Set<Label> getLabels() {
//    return labels;
//  }
  
  public Object getExternalFieldValue(String fieldId) {
    return externalFields.get(fieldId);
  }
  
  public Long getOriginalEstimate() {
    return originalEstimate;
  }
  
  public void setEstimate(Long estimate) {
    this.estimate = estimate;
  }
  
  public void setOriginalEstimate(Long originalEstimate) {
    this.originalEstimate = originalEstimate;
  }
  
//  public void setSubTaskObjects(Collection subTaskObjects) {
//    this.subTaskObjects = subTaskObjects;
//  }
  
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if ((o == null) || (getClass() != o.getClass())) {
      return false;
    }
    
    MyMockssue issue = (MyMockssue)o;
    
//    if (affectedVersions != null ? !affectedVersions.equals(affectedVersions) : affectedVersions != null) {
//      return false;
//    }
//    if (assignee != null ? !assignee.equals(assignee) : assignee != null) {
//      return false;
//    }
//    if (components != null ? !components.equals(components) : components != null) {
//      return false;
//    }
    if (created != null ? !created.equals(created) : created != null) {
      return false;
    }
    if (description != null ? !description.equals(description) : description != null) {
      return false;
    }
    if (dueDate != null ? !dueDate.equals(dueDate) : dueDate != null) {
      return false;
    }
    if (environment != null ? !environment.equals(environment) : environment != null) {
      return false;
    }
    if (estimate != null ? !estimate.equals(estimate) : estimate != null) {
      return false;
    }
//    if (fixVersions != null ? !fixVersions.equals(fixVersions) : fixVersions != null) {
//      return false;
//    }
//    if (genericValue != null ? !genericValue.equals(genericValue) : genericValue != null) {
//      return false;
//    }
    if (!id.equals(id)) {
      return false;
    }
//    if (issueType != null ? !issueType.equals(issueType) : issueType != null) {
//      return false;
//    }
    if (issueTypeId != null ? !issueTypeId.equals(issueTypeId) : issueTypeId != null) {
      return false;
    }
//    if (issueTypeObject != null ? !issueTypeObject.equals(issueTypeObject) : issueTypeObject != null) {
//      return false;
//    }
    if (key != null ? !key.equals(key) : key != null) {
      return false;
    }
    if (originalEstimate != null ? !originalEstimate.equals(originalEstimate) : originalEstimate != null) {
      return false;
    }
    if (parentId != null ? !parentId.equals(parentId) : parentId != null) {
      return false;
    }
//    if (priority != null ? !priority.equals(priority) : priority != null) {
//      return false;
//    }
    if (priorityId != null ? !priorityId.equals(priorityId) : priorityId != null) {
      return false;
    }
//    if (project != null ? !project.equals(project) : project != null) {
//      return false;
//    }
    if (projectId != null ? !projectId.equals(projectId) : projectId != null) {
      return false;
    }
//    if (reporter != null ? !reporter.equals(reporter) : reporter != null) {
//      return false;
//    }
    if (reporterId != null ? !reporterId.equals(reporterId) : reporterId != null) {
      return false;
    }
//    if (resolution != null ? !resolution.equals(resolution) : resolution != null) {
//      return false;
//    }
    if (resolutionId != null ? !resolutionId.equals(resolutionId) : resolutionId != null) {
      return false;
    }
//    if (resolutionObject != null ? !resolutionObject.equals(resolutionObject) : resolutionObject != null) {
//      return false;
//    }
//    if (securityLevel != null ? !securityLevel.equals(securityLevel) : securityLevel != null) {
//      return false;
//    }
    if (securityLevelId != null ? !securityLevelId.equals(securityLevelId) : securityLevelId != null) {
      return false;
    }
//    if (status != null ? !status.equals(status) : status != null) {
//      return false;
//    }
    if (statusId != null ? !statusId.equals(statusId) : statusId != null) {
      return false;
    }
//    if (statusObject != null ? !statusObject.equals(statusObject) : statusObject != null) {
//      return false;
//    }
    if (summary != null ? !summary.equals(summary) : summary != null) {
      return false;
    }
    if (timeSpent != null ? !timeSpent.equals(timeSpent) : timeSpent != null) {
      return false;
    }
    if (updated != null ? !updated.equals(updated) : updated != null) {
      return false;
    }
    if (votes != null ? !votes.equals(votes) : votes != null) {
      return false;
    }
    if (watches != null ? !watches.equals(watches) : watches != null) {
      return false;
    }
//    if (labels != null ? !labels.equals(labels) : labels != null) {
//      return false;
//    }
    if (workflowId != null ? !workflowId.equals(workflowId) : workflowId != null) {
      return false;
    }
//    if (projectObject != null ? !projectObject.equals(projectObject) : projectObject != null) {
//      return false;
//    }
    
    return true;
  }
  
  public String toString()
  {
    //return String.format("Mock Issue %d", new Object[] { id });
		StringBuilder sb = new StringBuilder();
		sb.append("key-> " + getKey());
		sb.append("Summary-> " + getSummary());
	return sb.toString();
   }


 }
