package br.com.softvaro.clonejira;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.codec.binary.Base64;

public class CreateIssueTest {

	private static final String TARGET_SERVER_BASE_URL = "http://ubuntuserver:8090/rest/api/latest/issue";
	private static final String TARGET_SERVER_USERNAME = "admin";
	private static final String TARGET_SERVER_PASSWORD = "softv@4dmin";
	private static final String TARGET_SERVER_PROJECT = "CLON";

	public static void main(String[] args) {
	    String line;
	    StringBuffer jsonString = new StringBuffer();
	    HttpURLConnection connection = null;
	    try {

	        URL url = new URL(TARGET_SERVER_BASE_URL);

			//String data = "{\"fields\" :{\"description\":\"Alinhar com TI quando passarão chamar os serviços de limpezas da Home ajustados para o padrão usado pelo Renato , já incluindo as limpezas da atualização cadastral, confpolitica e atribpolitica.\r\n\r\nVerificar qual capa de lote vamos trabalhar\",\"summary\":\"Agendar atualização do Cache Manager em produção com limpezas da Home via trigger - Renato\",\"customfield_10006\":\"HDIDC-3664\",\"issuetype\":{\"id\":\"2\"},\"priority\":{\"id\":\"2\"},\"project\":{\"key\":\"TIWS\"}}}";
	        //\"description\":\"Alinhar com TI quando passarao chamar os serviços de limpezas da Home ajustados para o padrao usado pelo Renato , ja incluindo as limpezas da atualizacao cadastral, confpolitica e atribpolitica.\r\n\r\nVerificar qual capa de lote vamos trabalhar\",
	        //,\"customfield_10006\":\"HDIDC-3664\",\"issuetype\":{\"id\":\"2\"},\"priority\":{\"id\":\"2\"}            // \\r\\n\\r\\n
	        //String data = "{\"fields\" :{\"project\":{\"key\":\"TIWS\"}, \"description\":\"Alinhar com TI quando passarão chamar os serviços de limpezas da Home ajustados para o padrão usado pelo Renato , já incluindo as limpezas da atualização cadastral, confpolitica e atribpolitica.\\r\\n\\r\\nVerificar qual capa de lote vamos trabalhar\",\"summary\":\"Agendar atualização do Cache Manager em produção com limpezas da Home via trigger - Renato\",\"issuetype\": {\"id\": \"10100\"},\"priority\":{\"id\":\"2\"}}}";
	        String data = "{\"fields\" :{\"description\":\"Alinhar com TI quando passarão chamar os serviços de limpezas da Home ajustados para o padrão usado pelo Renato , já incluindo as limpezas da atualização cadastral, confpolitica e atribpolitica.\\r\\n\\r\\nVerificar qual capa de lote vamos trabalhar\",\"summary\":\"Agendar atualização do Cache Manager em produção com limpezas da Home via trigger - Renato\",\"customfield_10006\":\"\",\"issuetype\":{\"id\":\"30000\"},\"priority\":{\"id\":\"2\"},\"project\":{\"key\":\"TIWS\"}}}";
	        //String data = "{\"fields\" :{\"project\":{ \"key\": \"TIWS\"},\"summary\": \"Another Issue created using REST API\",\"description\": \"Creating of an issue using project keys and issue type names using the REST API\",\"issuetype\": {\"name\": \"Task\"}}}";
	        connection = (HttpURLConnection) url.openConnection();
	        String authString = TARGET_SERVER_USERNAME + ":" + TARGET_SERVER_PASSWORD;
	        String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
	        connection.setRequestProperty("Authorization", "Basic " + authStringEnc);			

	        connection.setDoInput(true);
	        connection.setDoOutput(true);
	        connection.setRequestMethod("POST");
	        connection.setRequestProperty("Accept", "application/json");
	        connection.setRequestProperty("Content-Type", "application/json; Charset=UTF-8");
	        connection.connect();
	        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
	        writer.write(data);
	        writer.close();
	        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	        while ((line = br.readLine()) != null) {
	                jsonString.append(line);
	        }
	        br.close();
	        System.out.println("response code: " + connection.getResponseCode());
	        connection.disconnect();
	    } catch (Exception e) {
	        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
	        try {
				while ((line = br.readLine()) != null) {
				    System.out.println(line);
				}
		        br.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	e.printStackTrace();
	            throw new RuntimeException(e.getMessage());
	    }
	    System.out.println(jsonString.toString());
	}
}
