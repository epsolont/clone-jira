package br.com.softvaro.clonejira;

import com.google.gson.Gson;

/**
 * Class to represent data from a Jira issue (epic link)
 * @author colaborador
 *
 */
public class EpicFieldsSoftvaro extends Fields {

	//epic name
	private String customfield_10007; 
	//link para jira externo
	private String customfield_10900; 

	public EpicFieldsSoftvaro() {
		super();
	}

	@Override
	public void setEpicName(String epicName) {
		this.customfield_10007 = epicName;
	}
	
	@Override
	public void setLinkJiraExterno(String linkJiraExterno) {
		this.customfield_10900 = linkJiraExterno;
	}
	
	/**
	 * @return the customfield_10900
	 */
	public String getCustomfield_10900() {
		return customfield_10900;
	}

	/**
	 * @param customfield_10900 the customfield_10900 to set
	 */
	public void setCustomfield_10900(String customfield_10900) {
		this.customfield_10900 = customfield_10900;
	}

	
	/**
	 * @return the customfield_10007
	 */
	public String getCustomfield_10007() {
		return customfield_10007;
	}

	/**
	 * @param customfield_10900 the customfield_10900 to set
	 */
	public void setCustomfield_10007(String customfield_10007) {
		this.customfield_10007 = customfield_10007;
	}

}
