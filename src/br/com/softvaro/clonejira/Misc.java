package br.com.softvaro.clonejira;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

public class Misc {
	private static java.util.Properties p = new java.util.Properties();
    private static final String algorithm = "AES";
    private static Key key = null;
    private static Cipher cipher = null;
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

	    //Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
		
		// TODO Auto-generated method stub
		  String home = System.getProperty("user.home");
		  System.err.println("home: " + home);
		  File propsFile = new File(home, ".clonejira.properties");
		  InputStream is = new FileInputStream(propsFile);
		  p.load(is);
		  System.err.println("tserver. " + p.getProperty("targetServer"));
		  System.err.println("username. " + p.getProperty("username"));
		  p.setProperty("username", "emerson");

	      String encPassword = null;
	      String base64Key = p.getProperty("key");
	      cipher = Cipher.getInstance(algorithm);  
		  if (StringUtils.isNotEmpty(base64Key)) {
			  key = new SecretKeySpec(Base64.decodeBase64(base64Key), algorithm);
			  System.err.println("loaded key: " + key.getEncoded());
		  } else {
				KeyGenerator keyGen = KeyGenerator.getInstance(algorithm);
				keyGen.init(128);
				key = keyGen.generateKey();
				System.err.println("generated key: " + key.getEncoded());
		  }
		  
		  String keyBase64 = Base64.encodeBase64String(key.getEncoded());
		  System.err.println("key64: " + keyBase64);
		  p.setProperty("key", keyBase64);

		  String base64EncPwd = p.getProperty("password");
		  if (StringUtils.isNotEmpty(base64EncPwd)) {
			  System.err.println("dec pwd: " + decrypt(Base64.decodeBase64(base64EncPwd)));
		  } else {
			encPassword = new String(encrypt("teste"));
			System.err.println("enc: " + encPassword);
			String encPasswordBase64  = Base64.encodeBase64String(encrypt("teste"));
			System.err.println("encb64: " + encPasswordBase64);
			p.setProperty("password", encPasswordBase64);
		  }		  
		  
		  writeProp(p);
	}

	private static final void writeProp(Properties prop){ 
		OutputStream output = null;

		try {
			String home = System.getProperty("user.home");
			File propsFile = new File(home, ".clonejira.properties");
			output = new FileOutputStream(propsFile);
	
			// save properties to project root folder
			prop.store(output, null);
	
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
		}
	}
	

    private static final byte[] encrypt(String input)
        throws InvalidKeyException, 
               BadPaddingException,
               IllegalBlockSizeException {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] inputBytes = input.getBytes();
        return cipher.doFinal(inputBytes);
    }

    private static final String decrypt(byte[] encryptionBytes)
        throws InvalidKeyException, 
               BadPaddingException,
               IllegalBlockSizeException {
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] recoveredBytes = 
          cipher.doFinal(encryptionBytes);
        String recovered = 
          new String(recoveredBytes);
        return recovered;
    }
    private static void setUpEncription(String keyValue) throws Exception {
//        key = KeyGenerator.getInstance(algorithm).generateKey();
    	key = new SecretKeySpec(keyValue.getBytes(), algorithm);
        cipher = Cipher.getInstance(algorithm);
    }
}
