package br.com.softvaro.clonejira;

public class IssueCreateResponse {
	private String id;
	private String key;
	private String self;
	
	public IssueCreateResponse() {
		super();
	}
	//{"id":"12600","key":"TIWS-42","self":"http://ubuntuserver:8090/rest/api/latest/issue/12600"}
	//{"errorMessages":[],"errors":{"issuetype":"The issue type selected is invalid."}}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return the self
	 */
	public String getSelf() {
		return self;
	}
	/**
	 * @param self the self to set
	 */
	public void setSelf(String self) {
		this.self = self;
	}
}
