package br.com.softvaro.clonejira;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.security.InvalidKeyException;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import br.com.softvaro.clonejira.util.CypherUtil;
import br.com.softvaro.clonejira.util.JiraCloner;
import br.com.softvaro.clonejira.util.PropertyUtil;

/**
 * User interface of the Clone Jira project
 * @author colaborador
 *
 */
public class MainUI {
	private CypherUtil c;
	private String key;
	private JFrame frame;
	//private JTextField servidorOrigem;
	private JComboBox<MyComboItem[]>  servidorOrigem;
	private JTextArea jiraKey;
	private JTextArea log;
	private JTextField usuarioOrigem;
	private JTextField usuarioDestino;
	//private JTextField servidorDestino;
	private JComboBox<MyComboItem[]> servidorDestino;
	private JTextField projetoDestino;
	private JPasswordField passwordOrigem;
	private JPasswordField passwordDestino;
	private final Action actionSalvarConfig = new SwingActionSalvarConfig();
	private final Action actionClonarJira = new SwingActionClonarJira();

	private static final MyComboItem[] SOURCE_SERVERS_LIST;
	private static final MyComboItem[] TARGET_SERVERS_LIST;
	private static final DefaultComboBoxModel<MyComboItem[]> SOURCE_SERVERS_COMBO_MODEL;
	private static final DefaultComboBoxModel<MyComboItem[]> TARGET_SERVERS_COMBO_MODEL;
	/**
	 * Create the application.
	 */
	public MainUI(CypherUtil c) {
		initialize();
		this.c = c;
	}

	static {
		SOURCE_SERVERS_LIST = new MyComboItem[3];
		SOURCE_SERVERS_LIST[0] = new MyComboItem("http://hdixjira1:8080", "http://hdixjira1:8080");
		SOURCE_SERVERS_LIST[1] = new MyComboItem("http://jira.softvaro.com.br", "http://jira.softvaro.com.br");
//		SOURCE_SERVERS_LIST[2] = new MyComboItem("http://ubuntuserver:8090", "http://ubuntuserver:8090");		
		SOURCE_SERVERS_COMBO_MODEL = new DefaultComboBoxModel(SOURCE_SERVERS_LIST);
		
		TARGET_SERVERS_LIST = new MyComboItem[3];
		TARGET_SERVERS_LIST[0] = new MyComboItem("http://hdixjira1:8080", "http://hdixjira1:8080");
		TARGET_SERVERS_LIST[1] = new MyComboItem("http://jira.softvaro.com.br", "http://jira.softvaro.com.br");
//		TARGET_SERVERS_LIST[2] = new MyComboItem("http://ubuntuserver:8090", "http://ubuntuserver:8090");		
		TARGET_SERVERS_COMBO_MODEL = new DefaultComboBoxModel(TARGET_SERVERS_LIST);
	}
	/**
	 * @param args
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					MainUI window = new MainUI();
//					Properties p = PropertyUtil.getProperties();
//					window.show(p);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
	
	/**
	 * Show the user interface
	 */
	public void show(Properties p) {
		this.frame.setVisible(true);
		load(p);
		this.key = p.getProperty(PropertyUtil.KEY);
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 630);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JSplitPane splitPane = new JSplitPane();
		frame.getContentPane().add(splitPane, BorderLayout.NORTH);
		
		JPanel jiraLogPanel = new JPanel();
		log = new JTextArea(18,70);
		JScrollPane jiraLogPlaceholder = new JScrollPane(log);		
		log.setBorder(new LineBorder(Color.black));
		log.setEditable(false);
		jiraLogPanel.add(jiraLogPlaceholder);
		
		JButton clearLogButton = new JButton(new AbstractAction("   Limpar Log   ") {
		    public void actionPerformed(ActionEvent e) {
		       log.setText("");
		    }
		});
		
		jiraLogPanel.add(clearLogButton);

		jiraLogPanel.setBorder(BorderFactory.createTitledBorder("Log"));

		frame.getContentPane().add(jiraLogPanel, BorderLayout.CENTER);
		
		JPanel panelDireito = new JPanel();
		panelDireito.setLayout(new GridLayout(10, 2, 0, 0));		
		splitPane.setRightComponent(panelDireito);
		panelDireito.add(new JLabel(" "));  // just to add a blank line (margin)
		panelDireito.add(new JLabel(" "));
		
		JLabel lblServidorOrigem = new JLabel("   Servidor Origem");
		
//		servidorOrigem = new JTextField();
//		servidorOrigem.setColumns(30);
		
		servidorOrigem = new JComboBox<MyComboItem[]>();
		servidorOrigem.setModel(SOURCE_SERVERS_COMBO_MODEL);
		servidorOrigem.setEditable(false);

		JLabel lblUsuario = new JLabel("   Usuário");
		
		usuarioOrigem = new JTextField();
		usuarioOrigem.setColumns(30);

		JLabel lblSenha = new JLabel("   Senha");
		
		passwordOrigem = new JPasswordField();
		passwordOrigem.setText("abcdefg");

		JLabel lblServidorDestino = new JLabel("   Servidor Destino");
		
//		servidorDestino = new JTextField();
//		servidorDestino.setColumns(30);
		servidorDestino = new JComboBox<MyComboItem[]>();
		servidorDestino.setModel(TARGET_SERVERS_COMBO_MODEL);
		servidorDestino.setEditable(false);		
		
		JLabel lblProjetoDestino = new JLabel("   Projeto");
		
		projetoDestino = new JTextField();
		projetoDestino.setColumns(30);
		
		JLabel lblUsuarioDestino = new JLabel("   Usuário");
		
		usuarioDestino = new JTextField();
		usuarioDestino.setColumns(30);

		JLabel lblSenhaDestino = new JLabel("   Senha");
		
		passwordDestino = new JPasswordField();
		passwordDestino.setText("abcdefg");

		
		JButton btnSalvarConfig = new JButton("   Salvar Configuração  ");
		btnSalvarConfig.setAction(actionSalvarConfig);
		
		//panelDireito.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		panelDireito.add(lblServidorOrigem);
		panelDireito.add(servidorOrigem);
		panelDireito.add(lblUsuario);
		panelDireito.add(usuarioOrigem);
		panelDireito.add(lblSenha);
		panelDireito.add(passwordOrigem);
		panelDireito.add(lblServidorDestino);
		panelDireito.add(servidorDestino);
		panelDireito.add(lblProjetoDestino);
		panelDireito.add(projetoDestino);
		panelDireito.add(lblUsuarioDestino);
		panelDireito.add(usuarioDestino);
		panelDireito.add(lblSenhaDestino);
		panelDireito.add(passwordDestino);
		panelDireito.add(new JLabel("")); // empty placeholder, just to place button on 2nd column
		panelDireito.add(btnSalvarConfig);
		panelDireito.add(new JLabel(" "));  // just to add a blank line (margin)
		panelDireito.setBorder(BorderFactory.createTitledBorder("Configuração dos Servidores"));
		
		JPanel panelEsquerdo = new JPanel();
		panelEsquerdo.setLayout(new BoxLayout(panelEsquerdo, BoxLayout.PAGE_AXIS));
		
		JPanel panelEsquerdoExterno = new JPanel(); 
		JPanel jiraKeysPanel = new JPanel();
		panelEsquerdoExterno.add(panelEsquerdo, BorderLayout.NORTH);		
		splitPane.setLeftComponent(panelEsquerdoExterno);
		
		panelEsquerdo.add(new JLabel(" "));

		jiraKey = new JTextArea(11,30);
		JScrollPane jiraKeysPlaceholder = new JScrollPane(jiraKey);		
		jiraKey.setBorder(new LineBorder(Color.black));		
		jiraKeysPanel.add(jiraKeysPlaceholder);
		jiraKeysPanel.setBorder(BorderFactory.createTitledBorder("Jira(s) Key(s)"));
		panelEsquerdo.add(jiraKeysPanel);
		
		JButton btnClonarJiraButton = new JButton("Clonar Jira(s)");
		btnClonarJiraButton.setAction(actionClonarJira);
		panelEsquerdo.add(btnClonarJiraButton);
		
		
		splitPane.setDividerLocation(400);
	}
	
	/**
	 * Load the properties into the interface fields.
	 * @param p
	 */
	private void load(Properties p) {
		MyComboItem sourceServer = new MyComboItem(p.getProperty(PropertyUtil.ORIGIN_SERVER), p.getProperty(PropertyUtil.ORIGIN_SERVER));
		servidorOrigem.setSelectedItem(sourceServer);
		usuarioOrigem.setText(p.getProperty(PropertyUtil.ORIGIN_USER));		
		usuarioDestino.setText(p.getProperty(PropertyUtil.TARGET_USER));
		MyComboItem targetServer = new MyComboItem(p.getProperty(PropertyUtil.TARGET_SERVER), p.getProperty(PropertyUtil.TARGET_SERVER));
		servidorDestino.setSelectedItem(targetServer);
		//servidorDestino.setText(p.getProperty(PropertyUtil.TARGET_SERVER));
		projetoDestino.setText(p.getProperty(PropertyUtil.TARGET_PROJECT));
		try {
			passwordOrigem.setText(c.getDecryptedValue(PropertyUtil.ORIGIN_PASSWORD));
			passwordDestino.setText(c.getDecryptedValue(PropertyUtil.TARGET_PASSWORD));
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			log(e.getMessage());
		} catch (BadPaddingException e) {
			e.printStackTrace();
			log(e.getMessage());
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			log(e.getMessage());
		}
		
	}
	
	/**
	 * Action to be performed when the user clicks on the Salvar Configuração button
	 * @author colaborador
	 *
	 */
	private class SwingActionSalvarConfig extends AbstractAction {
		private static final long serialVersionUID = 22162743081323684L;
		public SwingActionSalvarConfig() {
			putValue(NAME, "Salvar Configuração");
			putValue(SHORT_DESCRIPTION, "Salvar Configuração");
		}
		public void actionPerformed(ActionEvent e) {
			Properties p = new Properties();
			p.setProperty(PropertyUtil.KEY, key);
			p.setProperty(PropertyUtil.ORIGIN_SERVER, ((MyComboItem)servidorOrigem.getSelectedItem()).getKey());
			p.setProperty(PropertyUtil.ORIGIN_USER, usuarioOrigem.getText());		
			p.setProperty(PropertyUtil.TARGET_USER, usuarioDestino.getText());
			p.setProperty(PropertyUtil.TARGET_SERVER, ((MyComboItem)servidorDestino.getSelectedItem()).getKey());
			p.setProperty(PropertyUtil.TARGET_PROJECT, projetoDestino.getText());
			try {
				p.setProperty(PropertyUtil.ORIGIN_PASSWORD, c.encryptValue(new String(passwordOrigem.getPassword())));
				p.setProperty(PropertyUtil.TARGET_PASSWORD, c.encryptValue(new String(passwordDestino.getPassword())));
			} catch (InvalidKeyException e1) {
				e1.printStackTrace();
				log(e1.getMessage());
			} catch (BadPaddingException e1) {
				e1.printStackTrace();
				log(e1.getMessage());
			} catch (IllegalBlockSizeException e1) {
				e1.printStackTrace();
				log(e1.getMessage());
			}
			
			PropertyUtil.writeProp(p);
		}
	}

	/**
	 * Action to be performed when the user clicks on the Clonar Jira button
	 * @author colaborador
	 *
	 */
	private class SwingActionClonarJira extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public SwingActionClonarJira() {
			putValue(NAME, "Clonar Jira(s)");
			putValue(SHORT_DESCRIPTION, "Clonar Jira(s)");
		}
		public void actionPerformed(ActionEvent e) {
		    Thread cloneThread = new Thread(new Runnable(){
		        public void run(){
					JiraCloner cloner = new JiraCloner(log);
					cloner.clone(((MyComboItem)servidorOrigem.getSelectedItem()).getValue(), 
							usuarioOrigem.getText(), 
							new String(passwordOrigem.getPassword()), 
							((MyComboItem)servidorDestino.getSelectedItem()).getValue(), 
							projetoDestino.getText(), usuarioDestino.getText(), 
							new String(passwordDestino.getPassword()), jiraKey.getText());
		        }
		    });
		    cloneThread.start();
		}
	}
	

	/**
	 * @param msg
	 */
	private void log(String msg) {
		log.append(msg);
		log.append("\n");
	}

}
