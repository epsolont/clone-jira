package br.com.softvaro.clonejira;

import com.google.gson.Gson;

/**
 * Class to represent data from a Jira issue
 * 
 * @author colaborador
 *
 */
public abstract class Fields {
    /*
     * {\"fields\": {\"project\":{ \"key\": \"TIS\"}, \"summary\": \"SISCAS-12377 Teste usando a API REST do Jira\",
     * \"description\": \"Criando uma tarefa usando a API REST do Jira\", \"status\": {\"id\": \"10100\"}, \"issuetype\": {\"name\": \"Task\"},
     * \"assignee\" : {\"name\": \"gisele.scaramel\"}, \"customfield_10006\": \"TIS-1\" } }
     */
    private String description;
    private String summary;
    // Epic Link
    // private String customfield_10006;
    // I cannot set this when creating an issue
    // private Status status;
    private IssueType issuetype;
    private Priority priority;
    private Project project;
    private Assignee assignee;

    public Fields() {
	// key = "";
	description = "";
	summary = "";
	// customfield_10006 = "";
	// issuetype = new IssueType("");
	// issuetype.setId("");
	// priority = new Priority("");
	// priority.setId("");
    }

    public abstract void setLinkJiraExterno(String linkJiraExterno);

    public abstract void setEpicName(String epicName);

    /**
     * @return
     */
    public String getDescription() {
	return description;
    }

    /**
     * @param description
     */
    public void setDescription(String description) {
	this.description = description;
    }

    /**
     * @return
     */
    public String getSummary() {
	return summary;
    }

    /**
     * @param summary
     */
    public void setSummary(String summary) {
	this.summary = summary;
    }

    /**
     * @return
     */
    public IssueType getIssuetype() {
	return issuetype;
    }

    /**
     * @param issuetype
     */
    public void setIssuetype(IssueType issuetype) {
	this.issuetype = issuetype;
    }

    /**
     * @return
     */
    public Priority getPriority() {
	return priority;
    }

    /**
     * @param priority
     */
    public void setPriority(Priority priority) {
	this.priority = priority;
    }

    /**
     * @return
     */
    public Project getProject() {
	return project;
    }

    /**
     * @param project
     */
    public void setProject(Project project) {
	this.project = project;
    }

    /**
     * @return the assignee
     */
    public Assignee getAssignee() {
	return assignee;
    }

    /**
     * @param assignee
     *            the assignee to set
     */
    public void setAssignee(Assignee assignee) {
	this.assignee = assignee;
    }

    @Override
    public String toString() {
	Gson gson = new Gson();
	return gson.toJson(this).toString();
    }

    /**
     * @author colaborador
     *
     */
    public class Status {
	private String id;

	public Status(String id) {
	    this.id = id;
	}

	public String getId() {
	    return id;
	}

	public void setId(String id) {
	    this.id = id;
	}
    }

    /**
     * @author colaborador
     *
     */
    public class IssueType {
	private String id;

	public IssueType(String id) {
	    this.id = id;
	}

	public String getId() {
	    return id;
	}

	public void setId(String id) {
	    this.id = id;
	}
    }

    /**
     * @author colaborador
     *
     */
    public class Assignee {
	private String name;

	public Assignee(String name) {
	    this.name = name;
	}

	public String getName() {
	    return name;
	}

	public void setName(String name) {
	    this.name = name;
	}
    }

    /**
     * @author colaborador
     *
     */
    public class Priority {
	private String id = "3";

	public Priority(String id) {
	    this.id = id;
	}

	public String getId() {
	    return id;
	}

	public void setId(String id) {
	    this.id = id;
	}
    }

    /**
     * @author colaborador
     *
     */
    public class Project {
	private String key;

	public Project(String key) {
	    this.key = key;
	}

	public String getKey() {
	    return key;
	}

	public void setKey(String key) {
	    this.key = key;
	}
    }

}
