package br.com.softvaro.clonejira;

/**
 * Class to represent data from a Jira issue
 * 
 * @author colaborador
 *
 */
public class FieldsSoftvaro extends Fields {

    /*
     * {\"fields\": {\"project\":{ \"key\": \"TIS\"}, \"summary\": \"SISCAS-12377 Teste usando a API REST do Jira\",
     * \"description\": \"Criando uma tarefa usando a API REST do Jira\", \"status\": {\"id\": \"10100\"}, \"issuetype\": {\"name\": \"Task\"},
     * \"customfield_10006\": \"TIS-1\" } }
     */

    // link para jira externo
    private String customfield_10900;

    public FieldsSoftvaro() {
	// key = "";
	super();
	// issuetype = new IssueType("");
	// issuetype.setId("");
	// priority = new Priority("");
	// priority.setId("");
    }

    public void setLinkJiraExterno(String linkJiraExterno) {
	this.customfield_10900 = linkJiraExterno;
    }

    /**
     * @return the customfield_10900
     */
    public String getCustomfield_10900() {
	return customfield_10900;
    }

    /**
     * @param customfield_10900
     *            the customfield_10900 to set
     */
    public void setCustomfield_10900(String customfield_10900) {
	this.customfield_10900 = customfield_10900;
    }

    @Override
    public void setEpicName(String epicName) {
	// does nothing epic name is supposed to be used only by epic links
    }
}
